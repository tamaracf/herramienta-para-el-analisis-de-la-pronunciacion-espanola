#!/usr/bin/env python3

import gi, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


tipoAnalisis = sys.argv[2].replace("_", " ")
titulo = sys.argv[1].replace("_", " ").upper() + "'S HISTORY"

# Se recupera la información necesaria para mostrar
f = open("Historico.txt", "r+")
info = f.read()
linea = info.split("|")

if tipoAnalisis != "1":
    f = open("Segmentos.txt", "r+")
    info = f.read()
    info = info.replace("-", ".").replace(":", "-").replace(".", ":")
    analisis = info.split("|")
    vueltas = len(analisis)-1


class ScrolledWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Praat Info: " + titulo)
        self.set_default_size(600, 200)
        self.connect("destroy", Gtk.main_quit)

        scrolledwindow = Gtk.ScrolledWindow()
        self.add(scrolledwindow)

        layout = Gtk.Layout()
        layout.set_size(10000, 10000)
        layout.set_vexpand(True)
        layout.set_hexpand(True)
        scrolledwindow.add(layout)

        hadjustment = layout.get_hadjustment()
        scrolledwindow.set_hadjustment(hadjustment)
        vadjustment = layout.get_vadjustment()
        scrolledwindow.set_vadjustment(vadjustment)

        grid = Gtk.Grid()
        grid.set_column_spacing(50)
        
        x=0
        y=2
        z=1
        res=[]
        palabra=[]

        # Análisis global
        if (tipoAnalisis == "Global analysis"):
            fecha = Gtk.Label(label="DATE")
            grid.attach(fecha, 0, 1, 1, 1)
            texto = Gtk.Label(label="PRONOUNCED TEXT")
            grid.attach(texto, 1, 1, 1, 1)
            tipoEnunciado = Gtk.Label(label="TYPE OF STATEMENT")
            grid.attach(tipoEnunciado, 2, 1, 1, 1)
            resultado = Gtk.Label(label="GLOBAL SIMILARITY")
            grid.attach(resultado, 3, 1, 1, 1)
            label = Gtk.Label(label="")
            grid.attach(label, 0, y, 1, 1)
            y = y+1

            for l in linea:
                if ((l != "\n") and (l != "")):
                    palabra = l.split("-")
                    i = 0
                    for p in palabra:
                        if (i != 0):
                            if (i==4):
                                p = p + "%"
                            p = p.replace(",", " ").replace("_", " ")
                            label = Gtk.Label(label=p)
                            grid.attach(label, x, y, 1, 1)
                            x=x+1
                        i=i+1
                    x=0
                    y=y+1

        # Análisis tendencia tonal
        elif (tipoAnalisis == "Tonal tendency analysis"):            
            a=0
            for l in linea:
                if ((l != "\n") and (l != "")):
                    fecha = Gtk.Label(label="DATE")
                    grid.attach(fecha, x, y, 1, 1)
                    x = x+1
                    texto = Gtk.Label(label="PRONOUNCED TEXT")
                    grid.attach(texto, x, y, 1, 1)
                    x = x+1
                    tipoEnunciado = Gtk.Label(label="TYPE OF STATEMENT")
                    grid.attach(tipoEnunciado, x, y, 1, 1)
                    x = x+1
                    resultado = Gtk.Label(label="CORRECT INTERVALS")
                    grid.attach(resultado, x, y, 1, 1)
                    x = 0
                    y = y+1

                    palabra = l.split("-")
                    i = 0
                    for p in palabra:
                        if (i != 0):
                            if ((i==4) or (i==5)):
                                p = p + "%"
                            p = p.replace(",", " ").replace("_", " ")
                            label = Gtk.Label(label=p)
                            grid.attach(label, x, y, 1, 1)
                            x=x+1
                        i=i+1

                x=0
                y=y+1
                label = Gtk.Label(label="")
                grid.attach(label, x, y, 1, 1)
                y=y+1

                i=1
                if a <= vueltas:
                    if ((analisis[a] != "\n") and (analisis[a] != "")):
                        label = Gtk.Label(label="WORDS:")
                        grid.attach(label, x, y, 1, 1)
                        x=x+1

                        resultados = []
                        i=1
                        segmentos = analisis[a].split("-")
                        longitud = len(segmentos)
                        for segmento in segmentos:
                            par = segmento.split("_")
                            if ((par != ['']) and (par != ['\n'])):
                                resultados.append(par[1])
                                valores= par[0]
                                valores = valores.split(":")
                                label = Gtk.Label(label=valores[0])
                                grid.attach(label, x, y, 1, 1)
                                x=x+2
                                if i == longitud-1:
                                    label = Gtk.Label(label=valores[1])
                                    grid.attach(label, x, y, 1, 1)
                                i += 1

                        x = 0
                        y = y+1
                        label = Gtk.Label(label="LOCAL DEVIATIONS:")
                        grid.attach(label, x, y, 1, 1)
                        x=x+2
                
                        for resultado in resultados: 
                            label = Gtk.Label(label=resultado.replace(":", "."))
                            grid.attach(label, x, y, 1, 1)
                            x=x+2
                        x=0
                        y=y+1
                        i=1
                        a+=1

                label = Gtk.Label(label="")
                grid.attach(label, x, y, 1, 1)
                y=y+1

                label = Gtk.Label(label="")
                grid.attach(label, x, y, 1, 1)
                y=y+1

        # Análisis intersilábico
        elif (tipoAnalisis == "Intersyllabic analysis"):
            a=0
            for l in linea:
                if ((l != "\n") and (l != "")):
                    fecha = Gtk.Label(label="DATE")
                    grid.attach(fecha, x, y, 1, 1)
                    x = x+1
                    texto = Gtk.Label(label="PRONOUNCED TEXT")
                    grid.attach(texto, x, y, 1, 1)
                    x = x+1
                    tipoEnunciado = Gtk.Label(label="TYPE OF STATEMENT")
                    grid.attach(tipoEnunciado, x, y, 1, 1)
                    x = x+1
                    resultado = Gtk.Label(label="GLOBAL SIMILARITY")
                    grid.attach(resultado, x, y, 1, 1)
                    x = x+1
                    diferencia = Gtk.Label(label="MEAN DIFFERENCE")
                    grid.attach(diferencia, x, y, 1, 1)
                    x = 0
                    y = y+1

                    palabra = l.split("-")
                    i = 0
                    for p in palabra:
                        if (i != 0):
                            if ((i==4) or (i==5)):
                                p = p + "%"
                            p = p.replace(",", " ").replace("_", " ")
                            label = Gtk.Label(label=p)
                            grid.attach(label, x, y, 1, 1)
                            x=x+1
                        i=i+1
                    x=0
                    y=y+1
                    i=1

                    label = Gtk.Label(label="")
                    grid.attach(label, x, y, 1, 1)
                    y=y+1

                    if a <= vueltas:
                        if analisis[a] != "\n":
                            resultados = []
                            i=1
                            segmentos = analisis[a].split("-")
                            label = Gtk.Label(label="SYLLABLES:")
                            grid.attach(label, x, y, 1, 1)
                            x=x+1

                            for segmento in segmentos:
                                par = segmento.split("_")
                                if ((par != ['']) and (par != ['\n'])):
                                    resultados.append(par[1])
                                    valor= par[0]
                                    label = Gtk.Label(label=valor)
                                    grid.attach(label, x, y, 1, 1)
                                    x=x+1

                            x=0
                            y=y+1
                            label = Gtk.Label(label="LOCAL DIFFERENCES:")
                            grid.attach(label, x, y, 1, 1)
                            x=x+1

                            for resultado in resultados: 
                                label = Gtk.Label(label=resultado)
                                grid.attach(label, x, y, 1, 1)
                                x=x+1
                            x=0
                            y=y+1
                            i=1
                            a+=1

                label = Gtk.Label(label="")
                grid.attach(label, x, y, 1, 1)
                y=y+1

                label = Gtk.Label(label="")
                grid.attach(label, x, y, 1, 1)
                y=y+1

        layout.add(grid)

win = ScrolledWindow()
win.show_all()

Gtk.main()