# FORMULARIO PARA OBTENER LA INFORMACIÓN RELEVANTE DEL DOCENTE
form Teacher characteristics
	sentence name_and_last_name
	sentence username
	sentence teaching_centre
endform

# ALMACENAR EL DOCENTE EN LA BASE DE DATOS
direccionInfoUsuarios$ = homeDirectory$ + "/Praat/Scripts/Info.txt"

nombreCompleto$ = replace$(name_and_last_name$, " ", "_", 0)
centroDocente$ = replace$(teaching_centre$, " ", "_", 0)
info$ = username$ + " " + nombreCompleto$ + " " + centroDocente$

writeFileLine: direccionInfoUsuarios$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/InsertarDatos.py 00"