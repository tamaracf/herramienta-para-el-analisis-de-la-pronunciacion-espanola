########## FORMULARIO PARA OBTENER LAS CARACTERÍSTICAS DE LOS AUDIOS A COMPARAR ##########
form Audio characteristics
	optionmenu type_of_statement: 1
		option Declarative
		option Desiderative
		option Enumerative
		option Exclamatory
		option Imperative
		option Partial interrogative
		option Alternative total interrogative
		option Polar total interrogative
	choice view_tonal_tendency_curves_in: 1
		button one chart
		button two charts
endform

direccionInfo$ = homeDirectory$ + "/Praat/Scripts/Info.txt"
writeFileLine: direccionInfo$, type_of_statement$
runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/SeleccionFicherosTonal.py"

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroAudioEstudiante.txt"
if fileReadable(direccionInfo$)
    ficheroAudioEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroAudioNativo.txt"
if fileReadable(direccionInfo$)
    ficheroAudioNativo$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroTextGridEstudiante.txt"
if fileReadable(direccionInfo$)
    ficheroTextGridEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroTextGridNativo.txt"
if fileReadable(direccionInfo$)
    ficheroTextGridNativo$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\DatosEstudiante.txt"
if fileReadable(direccionInfo$)
    datosEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\DatosNativo.txt"
if fileReadable(direccionInfo$)
    datosNativo$ = readFile$ (direccionInfo$)
endif

ficheroAudioEstudiante = Read from file: ficheroAudioEstudiante$
selectObject: ficheroAudioEstudiante
nombreAudio$ = selected$: "Sound"

ficheroTextGridEstudiante = Read from file: ficheroTextGridEstudiante$
selectObject: ficheroTextGridEstudiante
nombreTextGrid$ = selected$: "TextGrid"

# SE OBTIENEN LOS DATOS DEL AUDIO DEL ESTUDIANTE A ANALIZAR
duracionEstudiante$ = Get total duration
duracion = number(duracionEstudiante$)
plusObject: "Sound " + nombreAudio$

View & Edit

# Se recogen los segmentos en los que se divide el audio
include CalculoIntervalosEjeX.praat

# Se recogen los valores del pitch para cada segmento
include CalculoPitch.praat
direccionVectorEstudiante$ = homeDirectory$ + "/Praat/Scripts/vectorEstudianteAnalisisTonal.txt"
writeFileLine: direccionVectorEstudiante$, vector#

# Se calculan las tendencias tonales en cada segmento
include CalculoTendenciasTonales.praat
vectorEstudianteNormalizado# = vectorNormalizado#
tendenciaEstudiante# = tendencia#

ficheroAudioNativo = Read from file: ficheroAudioNativo$
selectObject: ficheroAudioNativo
nombreAudio$ = selected$: "Sound"

ficheroTextGridNativo = Read from file: ficheroTextGridNativo$
selectObject: ficheroTextGridNativo
nombreTextGrid$ = selected$: "TextGrid"

# SE OBTIENEN LOS DATOS DEL AUDIO Y DEL TEXTGRID DE REFERENCIA A COMPARAR
duracionReferencia$ = Get total duration
duracion = number(duracionReferencia$)
plusObject: "Sound " + nombreAudio$

View & Edit
include CalculoPitch.praat
direccionVectorReferencia$ = homeDirectory$ + "/Praat/Scripts/vectorReferenciaAnalisisTonal.txt"
writeFileLine: direccionVectorReferencia$, vector#
include CalculoTendenciasTonales.praat
vectorReferenciaNormalizado# = vectorNormalizado#
tendenciaReferencia# = tendencia#


########## SE CALCULAN LOS PORCENTAJES DE SIMILITUD GLOBAL Y LAS DESVIACIONES LOCALES A PARTIR DE LAS TENDENCIAS TONALES EN CADA PALABRA ##########
resultadoLocal$  = ""
count = 0

for i from 1 to intervalos
	if (tendenciaEstudiante# [i] = tendenciaReferencia# [i])

		# SE CALCULA LA PENDIENTE DE CADA CURVA
		if vectorEstudianteNormalizado# [i] >= vectorEstudianteNormalizado# [i+1]
			resEstudiante = vectorEstudianteNormalizado# [i] - vectorEstudianteNormalizado# [i+1]
		else
			resEstudiante = vectorEstudianteNormalizado# [i+1] - vectorEstudianteNormalizado# [i]
		endif

		if vectorReferenciaNormalizado# [i] >= vectorReferenciaNormalizado# [i+1]
			resReferencia = vectorReferenciaNormalizado# [i] - vectorReferenciaNormalizado# [i+1]
		else
			resReferencia = vectorReferenciaNormalizado# [i+1] - vectorReferenciaNormalizado# [i]
		endif

		if resReferencia = 0
			similitudLocal = 0.01
		else
			similitudLocal = resEstudiante / resReferencia
		endif

		if (similitudLocal = 0)
			similitudLocal = 1
		endif
		if (similitudLocal > 1)
			similitudLocalString$ = string$(similitudLocal)
			similitudLocal = similitudLocal - number (left$ (similitudLocalString$, (index (similitudLocalString$, ".") - 1)))
		endif

		if i = intervalos
			if similitudLocal = 1
				resultadoLocal$ = resultadoLocal$ + "C[0%]"
			else
				resultadoLocal$ = resultadoLocal$ + "C[" + percent$(1 - similitudLocal, 2) + "]"
			endif
		else
			if similitudLocal = 1
				resultadoLocal$ = resultadoLocal$ + "C[0%]" + "-"
			else
				resultadoLocal$ = resultadoLocal$ + "C[" + percent$(1 - similitudLocal, 2) + "]" + "-"
			endif
		endif
		
		count = count + 1
	else
		if i = intervalos
			resultadoLocal$ = resultadoLocal$ + "WRONG"
		else
			resultadoLocal$ = resultadoLocal$ + "WRONG" + "-"
		endif
	endif

endfor

aciertos$ = fixed$ ( (count / intervalos)*100, 2 )


# SE ALMACENA EL RESULTADO EN LA BASE DE DATOS
direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
intervalosEjeX$ = replace$ (intervalosEjeX$, " ", "", 0)
info$ = aciertos$ + ",fecha," + "grafica," + datosEstudiante$ + "," + datosNativo$ + "," + resultadoLocal$ + "," + intervalosEjeX$ + "," + string$(view_tonal_tendency_curves_in)
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "C:\msys64\mingw64.exe python " + homeDirectory$ + "/Praat/Scripts/SalidaAnalisisTendenciaTonal.py"