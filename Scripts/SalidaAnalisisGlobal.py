#!/usr/bin/env python3

import gi, sys, os

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


texto = sys.argv[1].replace("_", " ")
message = "The global result for the pronunciation of text '" + texto + "' is " + sys.argv[2] + "%."

class MessageDialogWindow(Gtk.Window):
    def __init__(self):
        dialog = Gtk.MessageDialog(
            parent=None,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Praat Info",
        )
        dialog.format_secondary_text(
            message
        )
        dialog.run()

        dialog.destroy()


win = MessageDialogWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()