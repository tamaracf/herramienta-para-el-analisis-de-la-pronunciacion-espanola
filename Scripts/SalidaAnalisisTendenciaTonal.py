#/usr/bin/env python3

import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


f = open("Info.txt", "r+")
datos = f.read()
f.close()

datos = datos.split(",")
texto1 = datos[9].replace("\n", "")
texto = texto1.split("_")
resLocal = datos[8].replace("[", "(").replace("]", ")").split("-")


class ScrolledWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_title("Praat Info: Tonal tendencies analysis")
        self.set_default_size(600, 200)
        self.connect("destroy", Gtk.main_quit)

        scrolledwindow = Gtk.ScrolledWindow()
        self.add(scrolledwindow)

        layout = Gtk.Layout()
        layout.set_size(10000, 200)
        layout.set_vexpand(True)
        layout.set_hexpand(True)
        scrolledwindow.add(layout)

        hadjustment = layout.get_hadjustment()
        scrolledwindow.set_hadjustment(hadjustment)
        vadjustment = layout.get_vadjustment()
        scrolledwindow.set_vadjustment(vadjustment)

        grid = Gtk.Grid()

        label00 = Gtk.Label(label="Un intervalo correcto es aquel en donde la tendencia tonal del estudiante coincide con la de referencia.")
        grid.attach(label00, 0, 0, 8, 1)

        label01 = Gtk.Label(label="Se indican los intervalos pronunciados correctamente (junto con su desviación respecto a la pronunciación de referencia) e incorrectamente:")
        grid.attach(label01, 0, 1, 13, 1)
        
        x=0
        for i in texto:
            label11 = Gtk.Label(label=i)

            if x==0:
                grid.attach(label11, x, 2, 1, 1)
            else:
                grid.attach(label11, x, 2, 1, 1)
            x=x+2

            label12 = Gtk.Label(label = i + "*")
            grid.attach(label12, x, 2, 1, 1)
            x=x+2

        x=1
        for i in resLocal:
            label2 = Gtk.Label(label=i.replace("_", " "))
            grid.attach(label2, x, 3, 1, 2)
            x=x+2
		
        label3 = Gtk.Label(label="")
        grid.attach(label3, 0, 4, 1, 1)
        label4 = Gtk.Label(label="")
        grid.attach(label4, 0, 5, 1, 1)
        label5 = Gtk.Label(label="Porcentaje de intervalos acertados: "+ str(datos[0]) + "%")
        grid.attach(label5, 0, 6, 2, 1)

        layout.add(grid)

window = ScrolledWindow()
window.show_all()

Gtk.main()

path = os.getcwd() + r'\Grafica.py '
os.execv("C:\msys64\mingw64.exe ", ["python ", path, " 0 ", texto1, datos[10]])