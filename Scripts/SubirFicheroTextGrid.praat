# SELECCIÓN DEL FICHERO .TEXTGRID A GUARDAR
fichero$ = chooseReadFile$: "Open a .texgrid file"

if fichero$ <> ""
	extension$ = right$ (fichero$, 8)
	if extension$ == "TextGrid"
		textgrid = Read from file: fichero$
	else
		exitScript: "File extension can't be different from .TextGrid, quitting."
	endif
else
	exitScript: "No .TextGrid file chosen, quitting."
endif

selectObject: textgrid


# FORMULARIO PARA OBTENER LOS PARÁMETROS DESCRIPTIVOS DEL FICHERO .TEXTGRID A ALMACENAR
form TextGrid characteristics
	choice type_tier: 1
		button Word
		button Syllable
	choice type_textgrid: 1
		button Native
		button Student
	optionmenu type_of_statement: 1
		option Declarative
		option Desiderative
		option Enumerative
		option Exclamatory
		option Imperative
		option Partial interrogative
		option Alternative total interrogative
		option Polar total interrogative
endform

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
info$ = type_tier$ + "," + type_textgrid$ + "," + type_of_statement$ + "," + fichero$
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/SubirFicheroTextGrid.py"