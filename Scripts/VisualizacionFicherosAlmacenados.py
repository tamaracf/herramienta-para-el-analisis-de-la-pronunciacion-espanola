import gi, ConsultarDatos

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

lista = []

f = open("Info.txt", "r+")
info = f.read()
f.close()

info = info.split("|")
orador = info[0]
info.remove(orador)

if (info != ['']):
    for i in info:
        if (i != []):
            datos = i.split(";")
            longitud = len(datos)

            tipoEnunciado = datos[0]
            valores = ("", "", "")

            audio = datos[1].replace(",", " - ")

            ConsultarDatos.main("18", tipoEnunciado)
            f = open("Info.txt", "r+")
            tipoEnunciado = f.read().strip()
            f.close()

            valores = (tipoEnunciado, audio, "")
            
            textgrids = datos[2].split(",")
            tg = ""
            if ((textgrids[0] != "None") and (textgrids[1] != "None")):
                ConsultarDatos.main("19", textgrids[1])
                f = open("Info.txt", "r+")
                textgrids[1] = f.read()
                f.close()
                
                tg = textgrids[1].replace(" ", "") + " - " + textgrids[0]
                tg = tg.strip()

            valores = (tipoEnunciado, audio, tg)
            
            lista.append(valores)
else:
    lista = [("", "", "")]


class TreeViewFilterWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Speaker " + orador)
        self.set_border_width(10)

        # Setting up the self.grid in which the elements are to be positioned
        self.grid = Gtk.Grid()
        self.grid.set_column_spacing(130)
        self.grid.set_row_homogeneous(True)
        self.add(self.grid)

        # Creating the ListStore model
        self.liststore = Gtk.ListStore(str, str, str)
        for statement_ref in lista:
            self.liststore.append(list(statement_ref))
        self.current_filter_statement = None

        # Creating the filter, feeding it with the liststore model
        self.statement_filter = self.liststore.filter_new()
        # setting the filter function, note that we're not using the
        self.statement_filter.set_visible_func(self.statement_filter_func)

        # creating the treeview, making it use the filter as a model, and adding the columns
        self.treeview = Gtk.TreeView(model=self.statement_filter)
        for i, column_title in enumerate(
            ["Statement type", "Audio", "TextGrid"]
        ):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            self.treeview.append_column(column)

        # creating buttons to filter by statement, and setting up their events
        self.buttons = list()
        for statement in ["Declarative", "Desiderative", "Enumerative", "Exclamatory", "Imperative", "Partial interrogative",
				"Alternative total interrogative", "Polar total interrogative", "None"]:
            button = Gtk.Button(label=statement)
            self.buttons.append(button)
            button.connect("clicked", self.on_selection_button_clicked)

        # setting up the layout, putting the treeview in a scrollwindow, and the buttons in a row
        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.set_vexpand(True)
        self.grid.attach(self.scrollable_treelist, 0, 0, 8, 10)
        self.grid.attach_next_to(
            self.buttons[0], self.scrollable_treelist, Gtk.PositionType.BOTTOM, 1, 1
        )
        count = 1
        for i, button in enumerate(self.buttons[1:]):
            if (count <= 4):
                self.grid.attach_next_to(
                    button, self.buttons[i], Gtk.PositionType.BOTTOM, 1, 1
                )
            else:
                self.grid.attach_next_to(
                    button, self.buttons[count-5], Gtk.PositionType.RIGHT, 1, 1
                )
            count = count + 1
        self.scrollable_treelist.add(self.treeview)

        self.show_all()

    def statement_filter_func(self, model, iter, data):
        """Tests if the statement in the row is the one in the filter"""
        if (
            self.current_filter_statement is None
            or self.current_filter_statement == "None"
        ):
            return True
        else:
            return model[iter][0] == self.current_filter_statement

    def on_selection_button_clicked(self, widget):
        """Called on any of the button clicks"""
        # we set the current statement filter to the button's label
        self.current_filter_statement = widget.get_label()
        # we update the filter, which updates in turn the view
        self.statement_filter.refilter()


win = TreeViewFilterWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()