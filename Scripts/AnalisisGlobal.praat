########## FORMULARIO PARA OBTENER LAS CARACTERÍSTICAS DE LOS AUDIOS A COMPARAR ##########
form Audio characteristics
	optionmenu type_of_statement: 1
		option Declarative
		option Desiderative
		option Enumerative
		option Exclamatory
		option Imperative
		option Partial interrogative
		option Alternative total interrogative
		option Polar total interrogative
endform

direccionInfo$ = homeDirectory$ + "/Praat/Scripts/Info.txt"
writeFileLine: direccionInfo$, type_of_statement$
runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/SeleccionFicherosGlobal.py"

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroAudioEstudiante.txt"
if fileReadable(direccionInfo$)
    ficheroAudioEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroAudioNativo.txt"
if fileReadable(direccionInfo$)
    ficheroAudioNativo$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\DatosEstudiante.txt"
if fileReadable(direccionInfo$)
    datosEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\DatosNativo.txt"
if fileReadable(direccionInfo$)
    datosNativo$ = readFile$ (direccionInfo$)
endif

ficheroAudioEstudiante = Read from file: ficheroAudioEstudiante$
selectObject: ficheroAudioEstudiante
nombreAudio$ = selected$: "Sound"

# SE OBTIENEN LOS DATOS DEL AUDIO DEL ESTUDIANTE A ANALIZAR
duracion$ = Get total duration
duracionEstudiante = number(duracion$)

View & Edit

# El audio se divide en 1000 puntos
puntos = 1000

tiempo = number(fixed$(duracionEstudiante, 4) ) 

# Se eliminan los silencios iniciales y finales
include prelimpiadoFicheroWAV.praat

vectorEstudiante# = vector#
direccionVectorEstudiante$ = homeDirectory$ + "/Praat/Scripts/vectorEstudianteAnalisisGlobal.txt"
writeFileLine: direccionVectorEstudiante$, vectorEstudiante#

direccionInfo$ = homeDirectory$ + "/Praat/Scripts/Info.txt"
writeFileLine: direccionInfo$, type_of_statement$

ficheroAudioNativo = Read from file: ficheroAudioNativo$
selectObject: ficheroAudioNativo
nombreAudio$ = selected$: "Sound"

# SE OBTIENEN LOS DATOS DEL AUDIO DEL NATIVO A ANALIZAR
duracion$ = Get total duration
duracionNativo = number(duracion$)

View & Edit

# El audio se divide en 1000 puntos
puntos = 1000

tiempo = number(fixed$(duracionNativo, 4) ) 

# Se eliminan los silencios iniciales y finales
include prelimpiadoFicheroWAV.praat

vectorReferencia# = vector#
direccionVectorReferencia$ = homeDirectory$ + "/Praat/Scripts/vectorReferenciaAnalisisGlobal.txt"
writeFileLine: direccionVectorReferencia$, vectorReferencia#


########## SE CALCULA EL PORCENTAJE DE SIMILITUD A PARTIR DE LA MEDIA DE LA DIFERENCIA PUNTO A PUNTO ##########
vectorDiferencias# = zero# (puntos)
count = 0

for i from 1 to puntos
	vectorDiferencias# [i] = 0
	if ( (vectorEstudiante# [i] <> undefined ) && (vectorReferencia# [i] <> undefined) ) 
		count = count + 1
		valor = abs(vectorEstudiante# [i] - vectorReferencia# [i])
		vectorDiferencias# [i] = valor  * 100
	endif
endfor

media$ = fixed$ (100 - (sum(vectorDiferencias#) / count), 2 )


# SE ALMACENA EL RESULTADO EN LA BASE DE DATOS
direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
info$ = media$ + ",fecha," + datosEstudiante$ + "," + datosNativo$
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/InsertarDatos.py 05"