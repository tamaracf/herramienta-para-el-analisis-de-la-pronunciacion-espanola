#!/usr/bin/env python3

import gi, sys


gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

if (sys.argv[1] == "00"):
    message = "Ya existe un usuario con el nombre " + sys.argv[2].replace("_", " ") + ". El nombre de usuario debe ser único."
elif (sys.argv[1] == "01"):
    message = "No existe ningún usuario con el nombre " + sys.argv[2].replace("_", " ") + "."

class MessageDialogWindow(Gtk.Window):
    def __init__(self):
        dialog = Gtk.MessageDialog(
            parent=None,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Praat Info",
        )
        dialog.format_secondary_text(
            message
        )
        dialog.run()

        dialog.destroy()

win = MessageDialogWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main() 