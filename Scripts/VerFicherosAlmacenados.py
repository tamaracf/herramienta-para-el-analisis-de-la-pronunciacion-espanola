#!/usr/bin/python3

import os, subprocess, ConsultarDatos


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
tipoOrador = f.read()
f.close()

ConsultarDatos.main("04", tipoOrador)

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

datos = tipoOrador + "," + orador

ConsultarDatos.main("17", datos)

path = os.getcwd() + "\VisualizacionFicherosAlmacenados.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)