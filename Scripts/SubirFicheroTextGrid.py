#!/usr/bin/python3

import os, subprocess, ConsultarDatos, GoogleDrive, InsertarDatos, random, parselmouth


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
lines = f.read()
f.close()

info = lines.split(",")
info[3] = info[3].replace("\n", "")

ConsultarDatos.main("04", info[1])

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

filtro = info[1] + "," + orador + "," + info[2]
ConsultarDatos.main("09", filtro)

path = os.getcwd() + "\SeleccionAudio.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
audio = f.read()
f.close()

x = audio.index(" - ")
texto = audio[0:x]
x = x+4
fechaAudio = audio[x:len(audio)]

if (info[0] == "1"):
	tier = 0
elif (info[0] == "2"):
	tier = 1

fichero = info[3]

if (info[1].replace("\n", "") == "Student"):
	path = os.getcwd()
	path = path.replace("Scripts", "Files/")
	if not (os.path.isdir(path)):
		os.mkdir(path)
	info[3] = path + str(random.randint(0, 1000000000000)) + ".TextGrid"

datos = "fecha" + "," + fechaAudio + "," + orador + "," + info[3] + "," + info[0] + "," +  info[1]


# SE ALMACENA EL TEXTGRID EN GOOGLE DRIVE Y EN LA BASE DE DATOS
# Nativo
if (info[1].replace("\n", "") == "Native"):
	f = open("Info.txt", "w+")
	f.write(datos)
	f.close()
	GoogleDrive.main("1")

# Estudiante
elif (info[1].replace("\n", "") == "Student"):
	f = open("Info.txt", "w+")
	f.write(datos)
	f.close()

	ruta = info[3]
	f = open("Fichero.txt", "w+")
	f.write(ruta + "," + fichero)
	f.close()

	output_string = parselmouth.praat.run_file('GuardarFichero.praat')

	InsertarDatos.main("04")