# SELECCIÓN DEL FICHERO .WAV A GUARDAR
fichero$ = chooseReadFile$: "Open a .wav file"

if fichero$ <> ""
	extension$ = right$ (fichero$, 3)
	if extension$ == "wav"
		audio = Read from file: fichero$
	else
		exitScript: "File extension can't be different from .WAV, quitting."
	endif
else
	exitScript: "No .WAV file chosen, quitting."
endif

selectObject: audio


# FORMULARIO PARA OBTENER LOS PARÁMETROS DESCRIPTIVOS DEL FICHERO .WAV A ALMACENAR
form Audio characteristics
	choice type_audio: 1
		button Native
		button Student
	optionmenu type_of_statement: 1
		option Declarative
		option Desiderative
		option Enumerative
		option Exclamatory
		option Imperative
		option Partial interrogative
		option Alternative total interrogative
		option Polar total interrogative
	sentence pronounced_text 
endform

nombreAudio$ = selected$("Sound")

if type_of_statement = 1
	tipoEnunciado$ = "Declarative"
	elsif type_of_statement = 2
		tipoEnunciado$ = "Desiderative" 
		elsif type_of_statement = 3
			tipoEnunciado$ = "Enumerative"
			elsif type_of_statement = 4
				tipoEnunciado$ = "Exclamatory" 
				elsif type_of_statement = 5
					tipoEnunciado$ = "Imperative" 
					elsif type_of_statement = 6
						tipoEnunciado$ = "Partial_interrogative" 
						elsif type_of_statement = 7
							tipoEnunciado$ = "Alternative_total_interrogative" 
							elsif type_of_statement = 8
								tipoEnunciado$ = "Polar_total_interrogative"
							endif
						endif
					endif
				endif
			endif
		endif
	endif
endif

if type_audio = 1
	tipo$ = "Native"
	elif type_audio = 2
		tipo$ = "Student"
	endif
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
info$ = type_audio$ + "," + type_of_statement$ + "," + pronounced_text$ + ",orador," + fichero$
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/SubirFicheroWAV.py"