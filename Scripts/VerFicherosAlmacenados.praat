# FORMULARIO PARA OBTENER LOS PARÁMETROS DESCRIPTIVOS DE LOS FICHEROS A ALMACENAR
form Speaker characteristics
	choice type_speaker: 1
		button Native
		button Student
endform

if type_speaker = 1
	tipo$ = "Native"
	elif type_speaker = 2
		tipo$ = "Student"
	endif
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
info$ = type_speaker$
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/VerFicherosAlmacenados.py"