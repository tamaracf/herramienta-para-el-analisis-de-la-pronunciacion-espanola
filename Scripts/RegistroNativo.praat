# FORMULARIO PARA OBTENER LA INFORMACIÓN RELEVANTE DEL NATIVO
form Local speaker characteristics
	sentence name_and_last_name
	choice gender: 1
		button Female
		button Male
	sentence age
	optionmenu level: 4
		option A1
		option A2
		option B1
		option B2
		option C1
		option C2
	sentence area
endform

# ALMACENAR EL NATIVO EN LA BASE DE DATOS
direccionInfoUsuarios$ = homeDirectory$ + "/Praat/Scripts/Info.txt"

nombreCompleto$ = replace$(name_and_last_name$, " ", "_", 0)
zonaGeografica$ = replace$(area$, " ", "_", 0)
info$ = nombreCompleto$ + " " + age$ + " " + zonaGeografica$ + " " + gender$ + " " + level$

writeFileLine: direccionInfoUsuarios$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/InsertarDatos.py 02"