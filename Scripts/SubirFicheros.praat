# SELECCIÓN DE FICHEROS A GUARDAR
i=1
audios$ = ""
select all sounds
numberOfSelectedSounds = numberOfSelected ("Sound ")
sounds# = selected# ("Sound")

for i to size (sounds#)
    selectObject: sounds# [i]
    name$ =  selected$ ("Sound")
    ruta$ = homeDirectory$ + "/Praat/Files/" + name$ + ".wav"
    Save as WAV file: ruta$
    if i == 1
        audios$ = ruta$
    else
        audios$ = audios$ + ";" + ruta$
    endif
endfor

i=1
textgrids$ = ""
select all textgrids
numberOfSelectedTextGrids = numberOfSelected ("TextGrid ")
textgrids# = selected# ("TextGrid")

for i to numberOfSelectedTextGrids
    selectObject: textgrids# [i]
    name$ =  selected$ ("TextGrid")
    ruta$ = homeDirectory$ + "/Praat/Files/" + name$ + ".TextGrid"
    Save as text file: ruta$
    if  i == 1
        textgrids$ = ruta$
    else
        textgrids$ = textgrids$ + ";" + ruta$
    endif
endfor

# FORMULARIO PARA OBTENER LOS PARÁMETROS DESCRIPTIVOS DE LOS FICHEROS A ALMACENAR
form Speaker characteristics
	choice type_speaker: 1
		button Native
		button Student
endform

if type_speaker = 1
	tipo$ = "Native"
	elif type_speaker = 2
		tipo$ = "Student"
	endif
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
info$ = type_speaker$ + ",orador," + audios$ + "," + textgrids$
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/SubirFicheros.py"