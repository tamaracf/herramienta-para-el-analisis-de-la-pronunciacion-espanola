# SE OBTIENEN LOS VALORES DE MAX Y MIN PITCH PARA CADA UNO DE LOS INTERVALOS
editor: "TextGrid " +  nombreTextGrid$

	info$ = TextGrid info
	intervalos = 2*number(extractLine$(info$, "Number of intervals:"))
	vector# = zero# (intervalos)

	Move cursor to: 0.0
	comienzo = 0.0
	final = Get end point of interval

	Select: comienzo, duracion
	min = Get minimum pitch
	max = Get maximum pitch

	Select: comienzo, final
	i = 1
	while (i <= intervalos)
		Move cursor to minimum pitch
		minPitch = Get pitch
		minSec = Get cursor

		Select: comienzo, final
		Move cursor to maximum pitch
		maxPitch = Get pitch
		maxSec = Get cursor

		if (minSec < maxSec)
			vector# [i] = minPitch
			vector# [i+1] = maxPitch
		else 
			vector# [i] = maxPitch
			vector# [i+1] = minPitch
		endif

		i = i + 2
		
		Select: comienzo, final

		Select next interval
		comienzo = Get starting point of interval
		final = Get end point of interval
	endwhile

endeditor