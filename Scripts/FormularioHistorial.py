#!/usr/bin/python

import gi, os

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class ComboBoxWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Select a speaker and the type of analysis:")

        self.set_border_width(10)
        self.set_default_size(500, 50)
        
        store = Gtk.ListStore(str)
        for orador in oradores:
            store.append([orador])

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        speaker_combo = Gtk.ComboBox.new_with_model(store)
        speaker_combo.connect("changed", self.on_speaker_combo_changed)
        renderer_text = Gtk.CellRendererText()
        speaker_combo.pack_start(renderer_text, True)
        speaker_combo.add_attribute(renderer_text, "text", 0)
        vbox.pack_start(speaker_combo, False, False, True)

        analysis = [
            "Global analysis",
            "Tonal tendency analysis",
            "Intersyllabic analysis"
        ]
        analysis_combo = Gtk.ComboBoxText()
        analysis_combo.set_entry_text_column(0)
        analysis_combo.connect("changed", self.on_analysis_combo_changed)
        for a in analysis:
            analysis_combo.append_text(a)

        vbox.pack_start(analysis_combo, False, False, 0)

        self.add(vbox)

    def on_speaker_combo_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            orador = model[tree_iter][0]
            path = os.getcwd() + "\Oradores.txt"
            f = open(path, "w+")
            f.write(str(orador))
            f.close()
            

    def on_analysis_combo_changed(self, combo):
        text = combo.get_active_text()
        if text is not None:
            path = os.getcwd() + "\Info.txt"
            f = open(path, "w+")
            f.write(str(text))
            f.close()

            path = os.getcwd() + "\Control.txt"
            f = open(path, "w+")
            f.write("OK")
            f.close()
            Gtk.Window.destroy(self)



def main():
    global oradores
    with open(os.getcwd() + r'\Oradores.txt', encoding='ISO-8859-1') as f:
        oradores = f.read()

    if (oradores.replace(" ", "").replace("\n", "") == "[]"):
        path = os.getcwd() + "/Control.txt"
        f = open(path, "w+")
        f.write("NO")
        f.close()

    oradores = oradores.replace("\n", "").split(",")

    win = ComboBoxWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()