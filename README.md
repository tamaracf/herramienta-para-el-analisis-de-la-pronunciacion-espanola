# HERRAMIENTA PARA EL ANÁLISIS DE LA PRONUNCIACIÓN ESPAÑOLA

## Tabla de Contenidos
1. [Información General](#general-info)
2. [Prerrequisitos](#prerequisites)
3. [Instalación](#installation)
4. [Autores](#authors)



## Información general
<a name="general-info"></a>
El propósito de este proyecto es la implementación de una serie de nuevas funcionalidades en la herramienta de análisis acústico Praat, con el objetivo de ofrecer retroalimentación para la mejora de las capacidades de comunicación orales en lengua española, atendiendo a aspectos fonéticos y prosódicos. 

* Altas y bajas de docentes: se cumplimenta un breve formulario con las características del docente a crear. A la hora de eliminar un docente es necesario eliminar previamente los estudiantes que tiene asociados. 

* Altas y bajas de oradores: nativos y estudiantes. Se cumplimenta un breve formulario con las características del orador a crear. Cada estudiante se asocia a un docente. 

* Gestión (almacenamiento y eliminado) de ficheros .WAV y .TextGrid tanto para nativos como para estudiantes. Se cumplimenta un breve formulario con las características del fichero a guardar. Cada fichero .TextGrid se asocia unívocamente con un .WAV. 

* Visualización de todos los ficheros almacenados: para un orador concreto, se permite visualizar todos los textgrids almacenados, junto con los audios correspondientes (no se muestran los audios que no tienen textgrids asociados) y el tipo de enunciado, y filtrar dicha información por tipo de enunciado.

* Análisis global: se comparan los valores de pitch de cada audio (nativo vs estudiante) en cada intervalo de tiempo para obtener una media de similitud. 

* Análisis de tendencias tonales: se comparan las tendencias del tono (ascendente, plana y descendente) en cada palabra pronunciada de cada audio (nativo vs estudiante) para conocer que intervalos se pronuncian correctamente. Para aquellos pronunciados correctamente, se devuelve la desviación de la curva tonal del estudiante respecto al nativo. Además, se muestra el porcentaje de intervalos acertados y una gráfica con las curvas de tendencia tonal para cada audio.

* Análisis intersilábico: se compara la diferencia media de pitch en cada sílaba. Además de mostrar la diferencia media local (para cada sílaba), se muestra la diferencia media global y la similitud global para el enunciado pronunciado, junto con la gráfica con las curvas de tendencia tonal para cada audio.

* Visualización del histórico de un estudiante: se muestran los resultados obtenidos en cada tipo de análisis ejecutado hasta el momento y una gráfica evolutiva que muestra el progreso del estudiante en la pronunciación de cada tipo de enunciado. 

BUG (será corregido en una versión posterior): en los campos del formulario no se deben introducir tildes ni carácteres especiales. Los directorios de donde se recuperan los ficheros para ser posteriormente almacenados en la base de datos no deben contener tildes ni carácteres especiales. Las anotaciones de los .TextGrids no deben contener tildes ni caracteres especiales. 



## Prerequisitos
<a name="prerequisites"></a>

Entre los requisitos técnicos (SO, herramientas, librerías y dependencias) que los usuarios necesitan tener preinstalados para poder utilizar PAFe, se resaltan los siguientes: 

1. Sistema operativo Windows, versión 10 o superior.

2. Lenguaje de programación [Python](https://www.python.org/downloads/), versión 3. Es importante marcar la casilla "Add Python x.x to PATH" en la ventana de configuración de la instalación. 
A continuación, se instalan las librerías necesarias desde el terminal de Windows:
    - pip install PyDrive
    - pip install psycopg2
    - pip install praat-parselmouth

3. Simulador de entorno de Linux en Windows: [msys2](https://www.msys2.org/). Una vez instalado, se comprueba el tipo de sistema que tiene nuestro portátil. Para ello, escribimos Acerca de tu PC en el buscador de la barra de tareas inferior de Windows y hacemos clic sobre esa opción. Se abre una pantalla en donde se nos muestran las especificaciones del dispositivo. Lo importante es saber si nuestro sistema es de 32bits o de 64bits. Si nuestro sistema es de 32bits, escribimos msys2 mingw 32-bits en el buscador de la barra de tareas inferior de Windows y hacemos clic sobre dicha herramienta. Por lo contrario, si nuestro sistema es de 64bits, escribimos msys2 mingw 64-bits y hacemos clic sobre dicha opción. Se nos abre un terminal y ejecutamos las siguientes instrucciones:
    - pacman -S mingw-w64-x86_64-python
    - pacman -S mingw-w64-x86_64-gtk3
    - pacman -S mingw-w64-x86_64-gobject-introspection
    - pacman -S mingw-w64-x86_64-python-numpy
    - pacman -S mingw-w64-x86_64-python-matplotlib
    - pacman -S mingw-w64-x86_64-python-psycopg2

4.	Gestor de base de datos [PostrgreSQL](https://www.postgresql.org/download/windows/) (contraseña = pass) e interfaz gráfica [PgAdmin](https://www.pgadmin.org/download/pgadmin-4-windows/). 

5.	Programa [Praat](https://www.fon.hum.uva.nl/praat/download_win.html), versión 6.

Una vez se tienen cumplidas dichas condiciones, se puede proceder con la instalación de PAFe.



## Instalación
<a name="installation"></a>
La naturaleza de esta solución software, un plugin, dificulta la creación de un único ejecutable que, con un doble click, realice automáticamente toda la instalación necesaria, ya que la solución no es independiente, dado que está subordinada a Praat y consiste en la adición de nuevas opciones de menú de Praat a través de las cuales se ejecutan los scripts correspondientes. Además, de existir ejecutables, serían necesarios uno por cada nueva funcionalidad. Por lo tanto, se requiere una instalación manual, cuyos pasos son los siguientes:

1. El [código fuente](https://gitlab.com/tamaracf/herramienta-para-el-analisis-de-la-pronunciacion-espanola) debe ser descargado y guardado en la carpeta en donde se encuentra Praat, por lo general, en la jerarquía de carpetas: C:\Users\nombreUsuario\Praat. 

2. Se abre el fichero Buttons5.ini, que contiene las rutas en donde se encuentran los scripts asociados a cada nuevo botón, y se sustituyen todas las rutas (C:\Users\username\Praat\Scripts\Fichero.praat) por las de nuestro portátil. 
Por lo general, bastará con sustituir username por nuestro nombre de usuario del portátil. 
Por ejemplo, si la ruta en la que tenemos instalado Praat se corresponde con C:\Users\Juan\Praat, se sustituye username por Juan, dando como resultado la dirección C:\Users\Juan\Praat\Scripts\... 
En cambio, si la ruta en la que se encuentra Praat no se corresponde con C:\Users\nombreUsuario\Praat, tenemos que adaptar el comienzo de la ruta en la que se encuentra el script asociado a cada botón (_________\Praat\Scritps\...) por la ruta en la que se encuentra Praat.

3. Se abre el terminal de Windows en la carpeta en la que se encuentran los scripts. Para esto, se escribe cmd en la barra de direcciones del explorador de archivos y se pulsa Enter, tal y como se ve en la imagen de la derecha.
En primer lugar, se da permiso a Google Drive para almacenar los ficheros correspondientes al nativo. Para ello, desde el terminal, se ejecuta la instrucción: python Config.py
A continuación, automáticamente se abre el inicio de sesión de Gmail en el navegador web. Seleccionamos Utilizar otra cuenta y nos registramos en el correo praatTesting@gmail.com con la contraseña contrAsena01. En la siguiente pantalla seleccionamos Continuar. En la siguiente pantalla seleccionamos los 2 servicios a los que Praat-PAFe desea tener acceso y pulsamos sobre Continuar. De esta manera se descargan las credenciales necesarias. 
Si se pasa mucho tiempo sin utilizar el plugin (más de 7 días) se deben renovar. Para ello, simplemente se elimina el fichero credentials.json que contiene las credenciales y se vuelve a repetir este último paso.

4. Se abre la aplicación PgAdmin buscándola en el buscador inferior de la barra de tareas de Windows (contraseña = pass). 
    - En File > Preferences > Paths > Binary Paths > PostgreSQL Binary Path (PostgreSQL 14) se escribe la ruta en la que se encuentra PostgreSQL. Por lo general, C:\Program Files\PostgreSQL\<version>\bin.

    - Se crea una base de datos con el nombre Praat, haciendo click sobre Databases > Create > Database…

    - Se restaura bd.backup que se encuentra en la carpeta Scripts haciendo click derecho sobre Praat > Restore…



## Autores
<a name="authors"></a>
- Tamara Couto Fernández
- Albina Sarymsakova
- Olinda Nelly Condori Fernández
- Patricia Martín Rodilla
