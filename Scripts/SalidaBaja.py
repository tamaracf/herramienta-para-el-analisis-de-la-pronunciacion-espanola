#!/usr/bin/env python3

import gi, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


if (sys.argv[1] == "00"): 
	message = "Teacher " + sys.argv[2] + " was deleted successfully."
elif (sys.argv[1] == "01"):
	message = message = "Student " + sys.argv[2].replace("_", " ") + " was deleted successfully."
elif (sys.argv[1] == "02"):
	message = message = "Local speaker " + sys.argv[2].replace("_", " ") + " was deleted successfully."
elif (sys.argv[1] == "03"):
    message = message = ".WAV file '" + sys.argv[2].replace("_", " ") + "' and .TextGrids associated were deleted successfully."
elif (sys.argv[1] == "05"):
    message = message = ".TextGrid file selected was deleted successfully."


class MessageDialogWindow(Gtk.Window):
    def __init__(self):
        dialog = Gtk.MessageDialog(
            parent=None,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Praat Info",
        )
        dialog.format_secondary_text(
            message
        )
        dialog.run()

        dialog.destroy()


win = MessageDialogWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()