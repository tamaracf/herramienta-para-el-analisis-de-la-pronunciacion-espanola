#!/usr/bin/python3

import os, subprocess, ConsultarDatos, GoogleDrive, InsertarDatos, random, parselmouth

from matplotlib.pyplot import text


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
lines = f.read()
f.close()

info = lines.split(",")
tipoOrador = info[0]
orador = info[1]
audios = info[2].split(";")
textgrids = info[3].replace("\n", "").split(";")

ConsultarDatos.main("04", tipoOrador)

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

i = 1
d = ""
datos = ""
valores = ""
longitud = len(audios)

for audio in audios:
    trozos = audio.split("/", -1)
    long = len(trozos)
    info = trozos[long-1]
    info = info.replace("_", " ").split(" - ")
    enunciado = info[0]
    texto = info[1].replace(".wav", "")

    if (tipoOrador == "Student"):
        path = os.getcwd()
        path = path.replace("Scripts", "Files/")
        if not (os.path.isdir(path)):
            os.mkdir(path)
        fichero = audio
        audio = path + str(random.randint(0, 1000000000000)) + ".WAV"

    d = orador + "," + "fecha" + "," + texto + "," + audio + "," + enunciado + "," + tipoOrador 
    datos = datos + ";" + d

    if (tipoOrador == "Native"):
        d = orador + "," + "fecha" + "," + texto + "," + audio + "," + enunciado + "," + tipoOrador + ",0"
        if ((i == longitud) and (textgrids == [])):
            d = orador + "," + "fecha" + "," + texto + "," + audio + "," + enunciado + "," + tipoOrador + ",-1"
    
    # SE ALMACENA EL AUDIO EN GOOGLE DRIVE Y EN LA BASE DE DATOS
    # Nativo
    if (tipoOrador == "Native"):
        f = open("Info.txt", "w+")
        f.write(d)
        f.close()
        
        GoogleDrive.main("0")

    # Estudiante
    elif (tipoOrador == "Student"):
        f = open("Info.txt", "w+")
        f.write(d)
        f.close()

        f = open("Fichero.txt", "w+")
        f.write(audio + "," + fichero)
        f.close()
        
        output_string = parselmouth.praat.run_file('GuardarFichero.praat')
    
    i = i + 1

    f = open("Info.txt", "r+")
    l = f.read()
    f.close()

    valores = valores + l + ";"
    
    if (tipoOrador == "Student"):
        f = open("Info.txt", "w+")
        f.write(datos)
        f.close()

if (tipoOrador == "Student"):
    InsertarDatos.main("08")


i = 1
d = ""
datos = ""
longitud = len(textgrids)

f = open("Info.txt", "r+")
lineas = f.read()
f.close()

lineas = lineas.split(";")
valores = valores.split(";")
for textgrid in textgrids:
    trozos = textgrid.split("/", -1)
    long = len(trozos)
    info = trozos[long-1]
    info = info.replace("_", " ").split(" - ")
    enunciado = info[0].replace("_", " ")
    texto = info[1].replace("_", " ")

    tier = info[2].replace(".TextGrid", "")
    if (tier == "Syllable"):
        typeTier = "0"
    elif (tier == "Word"):
        typeTier = "1"

    if (tipoOrador == "Native"):
        fechaAudio = ""
        for valor in valores:
            if (valor != ""):
                valor = valor.split(",")
                if ((texto == valor[2]) and (enunciado == valor[3])):
                    fechaAudio = valor[1]
    else:
        fechaAudio = ""
        for linea in lineas:
            if (linea != ""):
                linea = linea.split(",")
                if ((texto == linea[2]) and (enunciado == linea[3])):
                    fechaAudio = linea[1]
    fichero = textgrid

    if (tipoOrador == "Student"):
        path = os.getcwd()
        path = path.replace("Scripts", "Files/")
        if not (os.path.isdir(path)):
            os.mkdir(path)
        textgrid = path + str(random.randint(0, 1000000000000)) + ".TextGrid"

    d = "fecha" + "," + fechaAudio + "," + orador + "," + textgrid + "," + typeTier + "," +  tipoOrador
    datos = datos + ";" + d

    if (tipoOrador == "Native"):
        d = "fecha" + "," + fechaAudio + "," + orador + "," + textgrid + "," + typeTier + "," +  tipoOrador + ",0"
        if (i == longitud):
            d = "fecha" + "," + fechaAudio + "," + orador + "," + textgrid + "," + typeTier + "," +  tipoOrador + ",-1"
    
    # SE ALMACENA EL TEXTGRID EN GOOGLE DRIVE Y EN LA BASE DE DATOS
    # Nativo
    if (tipoOrador == "Native"):
        f = open("Info.txt", "w+")
        f.write(d)
        f.close()
        GoogleDrive.main("1")

    # Estudiante
    elif (tipoOrador == "Student"):
        f = open("Info.txt", "w+")
        f.write(d)
        f.close()

        ruta = textgrid
        f = open("Fichero.txt", "w+")
        f.write(ruta + "," + fichero)
        f.close()

        output_string = parselmouth.praat.run_file('GuardarFichero.praat')

    i = i + 1

    if (tipoOrador == "Student"):
        f = open("Info.txt", "w+")
        f.write(datos)
        f.close()

if (tipoOrador == "Student"):
    InsertarDatos.main("09")