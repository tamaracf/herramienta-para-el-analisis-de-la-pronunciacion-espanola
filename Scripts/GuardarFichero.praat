direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Fichero.txt"
info$ = ""

if fileReadable(direccionInfo$)
    info$ = readFile$ (direccionInfo$)
endif

x = rindex (info$, ",")
longitud = length (info$)
ruta$ = left$ (info$, x-1)

y = longitud - x
fichero$ = right$ (info$, y)

textgrid = Read from file: fichero$

selectObject: textgrid

Save as text file: ruta$