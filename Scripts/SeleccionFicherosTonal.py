#!/usr/bin/python3

import os, subprocess, ConsultarDatos, GoogleDrive


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
tipoOracion = f.read()
f.close()
tipoOracion = tipoOracion.replace("\n", "")

ConsultarDatos.main("04", "Student")

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

ConsultarDatos.main("08", orador)
f = open("Info.txt", "r+")
idOrador = f.read()
f.close()

filtro = "Student," + orador + "," + tipoOracion
ConsultarDatos.main("09", filtro)

path = os.getcwd() + "\SeleccionAudio.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
audio = f.read()
f.close()
x = audio.index(" - ")
texto = audio[0:x]
x = x+4
fechaAudio = audio[x:len(audio)]

datos = fechaAudio + "," + texto
ConsultarDatos.main("11", datos)

f = open("Info.txt", "r+")
fichero = f.read()
f.close()

if not (os.path.isfile("FicheroAudioEstudiante.txt")):
	open("FicheroAudioEstudiante.txt", "x")

f = open("FicheroAudioEstudiante.txt", "w+")
f.write(fichero)
f.close()

filtro = "Student," + idOrador + "," + tipoOracion + "," + texto + ",Word"
ConsultarDatos.main("14", filtro)

path = os.getcwd() + "\SeleccionTextGrid.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
textgrid = f.read()
f.close()

x = textgrid.index(" - ")
texto = textgrid[0:x]
x = x+4
fechas = textgrid[x:len(textgrid)]

y = fechas.index(" - ")
fechaAudio = fechas[0:y]
y = y+4
fechaTextGrid = fechas[y:len(fechas)]

if not (os.path.isfile("DatosEstudiante.txt")):
	open("DatosEstudiante.txt", "x")

datosEstudiante = idOrador + "," + fechaAudio
f = open("DatosEstudiante.txt", "w+")
f.write(datosEstudiante)
f.close()

datos = idOrador + "," + fechaAudio + "," + fechaTextGrid
ConsultarDatos.main("13", datos)

f = open("Info.txt", "r+")
textgridEstudiante = f.read()
f.close()

if not (os.path.isfile("FicheroTextGridEstudiante.txt")):
	open("FicheroTextGridEstudiante.txt", "x")

f = open("FicheroTextGridEstudiante.txt", "w+")
f.write(textgridEstudiante)
f.close()

f = open("Control.txt", "w+")
f.write("")
f.close()



ConsultarDatos.main("04", "Native")

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

filtro = "Native," + orador + "," + tipoOracion
ConsultarDatos.main("09", filtro)

path = os.getcwd() + "\SeleccionAudio.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
audio = f.read()
f.close()
x = audio.index(" - ")
texto = audio[0:x]
x = x+4
fechaAudio = audio[x:len(audio)]

datos = fechaAudio + "," + texto
ConsultarDatos.main("11", datos)

GoogleDrive.main("4")

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Info.txt", "r+")
fichero = f.read()
f.close()

if not (os.path.isfile("FicheroAudioNativo.txt")):
	open("FicheroAudioNativo.txt", "x")

f = open("FicheroAudioNativo.txt", "w+")
f.write(fichero)
f.close()

ConsultarDatos.main("07", orador)
f = open("Info.txt", "r+")
idOrador = f.read()
f.close()

filtro = "Native," + idOrador + "," + tipoOracion + "," + texto + ",Word"
ConsultarDatos.main("14", filtro)

path = os.getcwd() + "\SeleccionTextGrid.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
textgrid = f.read()
f.close()

x = textgrid.index(" - ")
texto = textgrid[0:x]
x = x+4
fechas = textgrid[x:len(textgrid)]

y = fechas.index(" - ")
fechaAudio = fechas[0:y]
y = y+4
fechaTextGrid = fechas[y:len(fechas)]

if not (os.path.isfile("DatosNativo.txt")):
	open("DatosNativo.txt", "x")

datosNativo = idOrador + "," + fechaAudio + "," + texto
f = open("DatosNativo.txt", "w+")
f.write(datosNativo)
f.close()

datos = idOrador + "," + fechaAudio + "," + fechaTextGrid
ConsultarDatos.main("13", datos)

GoogleDrive.main("4")

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Info.txt", "r+")
textgridNativo = f.read()
f.close()

if not (os.path.isfile("FicheroTextGridNativo.txt")):
	open("FicheroTextGridNativo.txt", "x")

f = open("FicheroTextGridNativo.txt", "w+")
f.write(textgridNativo)
f.close()

f = open("Control.txt", "w+")
f.write("")
f.close()