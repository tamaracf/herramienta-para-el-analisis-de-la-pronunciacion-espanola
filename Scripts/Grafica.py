#!/usr/bin/env python3

import sys, random, os, InsertarDatos
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


def normalize(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
		("Á", "A"),
        ("É", "E"),
        ("Í", "I"),
        ("Ó", "O"),
        ("Ú", "U"),
    )
    for a, b in replacements:
        s = s.replace(a, b)
    return s

op = sys.argv[1]
intervalos = sys.argv[2].split("_")
tipoGrafica = sys.argv[3].replace("\n", "")

# Se recupera la información de pitch necesaria para 
# construir las curvas
if (op == "0"):
    f = open(os.getcwd() + r'\vectorEstudianteAnalisisTonal.txt')
    vectorEstudiante = f.read().split(" ")
    f.close()

    f = open(os.getcwd() + r'\vectorReferenciaAnalisisTonal.txt')
    vectorReferencia = f.read().split(" ")
    f.close()

elif (op == "1"):
    f = open(os.getcwd() + r'\vectorEstudianteAnalisisIntersilabico.txt')
    vectorEstudiante = f.read().split(" ")
    f.close()

    f = open(os.getcwd() + r'\vectorReferenciaAnalisisIntersilabico.txt')
    vectorReferencia = f.read().split(" ")
    f.close()

longitud = len(vectorEstudiante)
x = []
i = 0
j = 0
valores = []
count = -1

while (i < len(intervalos)):
	v = normalize(intervalos[i])
	valores.append(v.lower())
	valores.append(v.lower() + "*")
	x.append(count + 1)
	x.append(count + 2)
	count = count + 2
	i += 1

estudiante = []
for i in range(longitud):
	estudiante.append(round(float(vectorEstudiante[i]), 2))

referencia = []
for i in range(longitud):
	referencia.append(round(float(vectorReferencia[i]), 2))

# Se construye la gráfica
if (tipoGrafica == "1"):
    plt.plot(x, estudiante, color='g')
    plt.plot(x, referencia, color='orange')

    plt.xticks(x, valores, rotation='vertical')
    plt.title('Tonal tendencies')
    plt.xlabel('Intervals')
    plt.ylabel('Pitch (Hz)')

    leyenda_Estudiante = mpatches.Patch(color='g', label='Student')
    leyenda_Referencia = mpatches.Patch(color='orange', label='Reference')
    plt.legend(handles=[leyenda_Estudiante, leyenda_Referencia])

elif (tipoGrafica == "2"):
    fig, ax = plt.subplots(2, sharex=True, sharey=True)
    fig.suptitle("Tonal tendencies")

    ax[0].plot(x, estudiante, color='g', label='Student')
    ax[0].legend(loc="upper right")
    ax[0].set_ylabel("Pitch (Hz)")

    ax[1].plot(x, referencia, color='orange', label='Reference')
    ax[1].legend(loc="upper right")
    ax[1].set_ylabel("Pitch (Hz)")

    plt.xticks(x, valores, rotation='vertical')
    plt.xlabel('Intervals')

ruta = os. getcwd().replace("Scripts", "Files/")
nombre = str(random.randint(0, 1000000000000))

# Se guarda la imagen para almacenarla posteriormente en la base de datos
plt.savefig(ruta + nombre + ".png")

f = open(os. getcwd() + "/Grafica.txt", "w+")
f.write(ruta + nombre + ".png")
f.close()

plt.show()

f = open(os.getcwd() + r'\Info.txt', "r+")
info = f.read().split(",")
f.close()

longitud = len(info)
eliminar = info[longitud-1]
info.remove(eliminar)

d = ""
count = 0
for i in info:
    if (count == 0):
        d = d + i
    else:
        d = d + "," + i
    count = count + 1

f = open(os.getcwd() + r'\Info.txt', "w+")
f.write(d)
f.close()

if (op == "0"):
    InsertarDatos.main("06")
elif (op == "1"):
    InsertarDatos.main("07")