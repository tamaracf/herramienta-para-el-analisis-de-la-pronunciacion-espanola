#!/usr/bin/python3

import os, subprocess, ConsultarDatos, EliminarDatos, GoogleDrive


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
lines = f.read()
f.close()

info = lines.split(",")
info[1] = info[1].replace("_", " ").replace("\n", "")

ConsultarDatos.main("04", info[0])

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

filtro = info[0] + "," + orador + "," + info[1]
ConsultarDatos.main("09", filtro)

path = os.getcwd() + "\SeleccionAudio.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
audio = f.read()
f.close()

x = audio.index(" - ")
texto = audio[0:x]
x = x+4
fechaAudio = audio[x:len(audio)]

if (info[0] == "Native"):
	ConsultarDatos.main("07", orador.replace("_", " "))
elif (info[0] == "Student"):
	ConsultarDatos.main("08", orador.replace("_", " "))

f = open("Info.txt", "r+")
idOrador = f.read()
f.close()

d = fechaAudio + "," + texto
ConsultarDatos.main("11", d)
f = open("Info.txt", "r+")
fichero = f.read()
f.close()

if (info[0] == "Native"):
	GoogleDrive.main("2")
elif (info[0] == "Student"):
	os.remove(fichero)

datos = idOrador + "," + fechaAudio + "," + texto + "," + info[0]
f = open("Info.txt", "w+")
f.write(datos)
f.close()

EliminarDatos.main("03")