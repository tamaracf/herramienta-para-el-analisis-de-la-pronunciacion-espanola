#!/usr/bin/env python3

import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


# Se recogen los resultados para poder construir la gráfica
f = open("Historico.txt", "r+")
info = f.read()
lineas = info.split("|")

i=0
declarativeX=[]
desiderativeX=[]
enumerativeX=[]
exclamatoryX=[]
imperativeX=[]
partialX=[]
alternativeX=[]
polarX=[]
declarativeY=[]
desiderativeY=[]
enumerativeY=[]
exclamatoryY=[]
imperativeY=[]
partialY=[]
alternativeY=[]
polarY=[]
tipo=""

for linea in lineas:
	if ((linea != ['']) and (linea != ['\n'])):
		palabra = linea.split("-")

		if ((palabra != ['']) and (palabra != ['\n'])):
			fecha = palabra[1]
			fecha = fecha[2:16]
			res = float(palabra[4])
			tipo = palabra[3]
			
			if tipo == "Declarative":
				declarativeY.append(res)
				declarativeX.append(fecha)
			elif tipo == "Desiderative":
				desiderativeY.append(res)
				desiderativeX.append(fecha)
			elif tipo == "Enumerative":
				enumerativeY.append(res)
				enumerativeX.append(fecha)
			elif tipo == "Exclamatory":
				exclamatoryY.append(res)
				exclamatoryX.append(fecha)
			elif tipo == "Imperative":
				imperativeY.append(res)
				imperativeX.append(fecha)
			elif tipo == "Partial interrogative":
				partialY.append(res)
				partialX.append(fecha)
			elif tipo == "Alternative total interrogative":
				alternativeY.append(res)
				alternativeX.append(fecha)
			elif tipo == "Polar total interrogative":
				polarY.append(res)
				polarX.append(fecha)


plt.title('Progress')
plt.xlabel('Date')
plt.ylabel('Pronunciation similarity (%)')

plt.plot(declarativeX, declarativeY, color='b', marker='o', markerfacecolor='b', markersize=10)
leyenda_Declarative = mpatches.Patch(color='b', label='Declarative')

plt.plot(desiderativeX, desiderativeY, color='g', marker='o', markerfacecolor='g', markersize=10)
leyenda_Desiderative = mpatches.Patch(color='g', label='Desiderative')

plt.plot(enumerativeX, enumerativeY, color='r', marker='o', markerfacecolor='r', markersize=10)
leyenda_Enumerative = mpatches.Patch(color='r', label='Enumerative')

plt.plot(exclamatoryX, exclamatoryY, color='c', marker='o', markerfacecolor='c', markersize=10)
leyenda_Exclamatory = mpatches.Patch(color='c', label='Exclamatory')

plt.plot(imperativeX, imperativeY, color='m', marker='o', markerfacecolor='m', markersize=10)
leyenda_Imperative = mpatches.Patch(color='m', label='Imperative')

plt.plot(partialX, partialY, color='y', marker='o', markerfacecolor='y', markersize=10)
leyenda_Partial = mpatches.Patch(color='y', label='Partial interrogative')

plt.plot(alternativeX, alternativeY, color='k', marker='o', markerfacecolor='k', markersize=10)
leyenda_Alternative = mpatches.Patch(color='k', label='Alternative total interrogative')

plt.plot(polarX, polarY, color='grey', marker='o', markerfacecolor='grey', markersize=10)
leyenda_Polar = mpatches.Patch(color='grey', label='Polar total interrogative')

plt.legend(handles=[leyenda_Declarative, leyenda_Desiderative, leyenda_Enumerative, leyenda_Exclamatory, 
					leyenda_Imperative, leyenda_Partial, leyenda_Alternative, leyenda_Polar])
					
plt.show()