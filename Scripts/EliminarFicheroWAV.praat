# FORMULARIO PARA OBTENER LA INFORMACIÓN RELEVANTE DEL AUDIO
form Audio characteristics
	choice type_audio: 1
		button Native
		button Student
	optionmenu type_of_statement: 1
		option Declarative
		option Desiderative
		option Enumerative
		option Exclamatory
		option Imperative
		option Partial interrogative
		option Alternative total interrogative
		option Polar total interrogative
endform


# ELIMINAR EL AUDIO DE LA BASE DE DATOS
direccionInfo$ = homeDirectory$ + "/Praat/Scripts/Info.txt"
info$ = type_audio$ + "," + type_of_statement$

writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/EliminarFicheroWAV.py"