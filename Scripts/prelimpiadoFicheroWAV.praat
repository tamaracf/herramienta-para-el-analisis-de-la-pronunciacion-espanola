editor: "Sound " +  nombreAudio$
	vector# =  zero#(puntos)

	# OBTENEMOS EL PRIMER SEGUNDO VÁLIDO, AQUEL CUYO PITCH TIENE VALOR
	x = 0
	y = 0
	parar = 0
	ud = 0.0001	
	while (x+ud <= tiempo && parar = 0)
		Select: x, x+ud
		pitch = Get pitch
		if (pitch <> undefined)
			y = x
			parar = 1
		endif
		x = x + ud
	endwhile

	# OBTENEMOS EL ÚLTIMO SEGUNDO VÁLIDO, AQUEL CUYO PITCH TIENE VALOR
	x = tiempo
	z = 0
	parar = 0
	while (x-ud >= 0 && parar = 0)
		Select: x-ud, x
		pitch = Get pitch
		if (pitch <> undefined)
			z = x
			parar = 1
		endif
		x = x - ud
	endwhile

	# CALCULAMOS LA DURACIÓN ÚTIL DEL AUDIO
	duracion = z - y
	unidad = duracion / puntos

	Select: y, z
	min = Get minimum pitch
	max = Get maximum pitch

	# OBTENEMOS UN VECTOR CON EL PITCH NORMALIZADO PARA CADA PUNTO 
	i = 1
	while (i <= puntos)
		Select: y, y + unidad
		pitch = Get pitch
		if pitch <> undefined
			# SE NORMALIZAN LOS VALORES DE PITCH
			vector# [i] = (pitch - min) / (max - min)
		else
			vector# [i] = pitch
		endif
		y = y + unidad
		i = i + 1
	endwhile

endeditor