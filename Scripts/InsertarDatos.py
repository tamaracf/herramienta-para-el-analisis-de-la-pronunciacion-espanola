#!/usr/bin/python3

import psycopg2, sys, os, ConsultarDatos
from datetime import datetime
import time


# Insertar información en la base de datos
def InsertarDatos(op, info):
    datos = []
    sql = ""
    
    if (op == "00"):
        info = info.split(" ")
        datos = info
        err = 0
        
        datos[1] = datos[1].replace("_", " ")
        datos[2] = datos[2].replace("_", " ").replace("\n", "")
        sql = """INSERT INTO "Docente" ("nombreUsuario", "nombreCompleto", "centroDocente") VALUES (%s, %s, %s) RETURNING id;"""

    elif (op == "01"):        
        info = info.split(",")
        datos = info
        err = 1
        
        datos[1] = datos[1].replace("_", " ")
        f = open("Docentes.txt", "r+")
        datos[3] = f.read()
        f.close()
        ConsultarDatos.main("03", datos[3])
        f = open("Info.txt", "r+")
        datos[3] = f.read()
        f.close()
        ConsultarDatos.main("01", datos[4])
        f = open("Info.txt", "r+")
        datos[4] = f.read()
        f.close()
        datos[5] = datos[5].replace("\n", "")
        ConsultarDatos.main("02", datos[5])
        f = open("Info.txt", "r+")
        datos[5] = f.read()
        f.close()
        sql = """INSERT INTO "Estudiante" ("nombreUsuario", "nombreCompleto", edad, "idDocente", genero, nivel) VALUES (%s, %s, %s, %s, %s, %s) RETURNING id;"""
        
    elif (op == "02"):
        info = info.split(" ")
        datos = info
        err = 2
        
        datos[0] = datos[0].replace("_", " ")
        datos[2] = datos[2].replace("_", " ")
        ConsultarDatos.main("01", datos[3])
        f = open("Info.txt", "r+")
        datos[3] = f.read()
        f.close()
        datos[4] = datos[4].replace("\n", "")
        ConsultarDatos.main("02", datos[4])
        f = open("Info.txt", "r+")
        datos[4] = f.read()
        f.close()
        sql = """INSERT INTO "Nativo" ("nombreCompleto", edad, "zonaGeografica", genero, nivel) VALUES (%s, %s, %s, %s, %s) RETURNING id;"""
    
    elif (op == "03"):
        err = 3
        info = info.split(",")
        
        if (len(info) == 7):
            señal = info[6]
            
            if (señal == "-1"):
                err = 9
            elif (señal == "0"):
                err = 20

            info.remove(señal)

        datos = info
        
        datos[0] = datos[0].replace("_", " ")
        if (datos[5].replace("\n", "") == "Native"):
            ConsultarDatos.main("07", datos[0])
        elif (datos[5].replace("\n", "") == "Student"):
            ConsultarDatos.main("08", datos[0])
        f = open("Info.txt", "r+")
        datos[0] = f.read()
        f.close()

        datos[1] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        datos[2] = datos[2].replace("_", " ")
        
        tipoEnunciado = datos[4]
        ConsultarDatos.main("05", datos[4])
        f = open("Info.txt", "r+")
        datos[4] = f.read()
        f.close()

        datos[5] = datos[5].replace("\n", "")
        ConsultarDatos.main("06", datos[5])
        f = open("Info.txt", "r+")
        datos[5] = f.read()
        f.close()

        d = datos[0] + "," + datos[1] + "," + datos[2] + "," + tipoEnunciado

        f = open("Info.txt", "w+")
        f.write(d)
        f.close()

        sql = """INSERT INTO "Audio" ("idOrador", "fechaAlmacenamiento", texto, fichero, "tipoEnunciado", "tipoAudio") 
                VALUES (%s, %s, %s, %s, %s, %s) RETURNING "idOrador";"""

    elif (op == "04"):
        err = 3
        info = info.split(",")

        if (len(info) == 7):
            señal = info[6]

            if (señal == "-1"):
                err = 9
            elif (señal == "0"):
                err = 20

        tipo = info[5]

        if (len(info) == 7):
            info.remove(señal)

        info.remove(tipo)
        datos = info
        
        datos[0] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        if (tipo.replace("\n", "") == "Native"):
            ConsultarDatos.main("07", datos[2])
        elif (tipo.replace("\n", "") == "Student"):
            ConsultarDatos.main("08", datos[2])
        f = open("Info.txt", "r+")
        datos[2] = f.read()
        f.close()
    
        if (len(datos[4]) > 1 ):
            ConsultarDatos.main("10", datos[4])
            f = open("Info.txt", "r+")
            datos[4] = f.read()
            f.close()
            
        sql = """INSERT INTO "TextGrid" ("fechaAlmacenamientoTextGrid", "fechaAlmacenamientoAudio", "idOrador", fichero, tipo) 
                VALUES (%s, %s, %s, %s, %s) RETURNING "idOrador";"""

    elif (op == "05"):
        info = info.split(",")
        info[6] = info[6].replace("\n", "")
        texto = info[6]
        info.remove(texto)
        datos = info
        err = 5
        
        datos[1] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        
        sql = """INSERT INTO "AnalisisGlobal" ("similitudGlobal", fecha, "idReferencia", 
                "fechaAlmacenamientoAudioReferencia", "idEstudiante", "fechaAlmacenamientoAudioEstudiante") 
                VALUES (%s, %s, %s, %s, %s, %s) RETURNING id;"""
    
    elif (op == "06"):
        info = info.split(",")

        f = open(os. getcwd() + "/Grafica.txt", "r+")
        info[2] = f.read()
        f.close()
        
        info[9] = info[9].replace("\n", "")
        texto = info[7]
        resLocal = info[8]
        segmentos = info[9].replace(" ", "")
        
        info.remove(texto)
        info.remove(resLocal)
        info.remove(segmentos)

        datos1 = info
        datos = resLocal + "," + segmentos

        err = 6
        
        datos1[1] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        
        sql = """INSERT INTO "AnalisisTendenciaTonal" ("similitudGlobal", fecha, grafica, "idEstudiante", 
                "fechaAlmacenamientoAudioEstudiante", "idReferencia", "fechaAlmacenamientoAudioReferencia") 
                VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING id;"""        

    elif (op == "07"):
        info = info.split(",")
        
        f = open(os. getcwd() + "/Grafica.txt", "r+")
        info[2] = f.read()
        f.close()
        
        info[10] = info[10].replace("\n", "")
        texto = info[7]
        resLocal = info[9]
        segmentos = info[10].replace(" ", "")
        
        info.remove(texto)
        info.remove(resLocal)
        info.remove(segmentos)

        datos1 = info
        datos = resLocal + "," + segmentos

        err = 7
        
        datos1[1] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        sql = """INSERT INTO "AnalisisIntersilabico" ("similitudGlobal", fecha, grafica, "idEstudiante", "fechaAlmacenamientoAudioEstudiante", 
                "idReferencia", "fechaAlmacenamientoAudioReferencia", "diferenciaMedia") VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING id;"""

    elif (op == "08"):
         sql = """INSERT INTO "Audio" ("idOrador", "fechaAlmacenamiento", texto, fichero, "tipoEnunciado", "tipoAudio") 
                VALUES (%s, %s, %s, %s, %s, %s) RETURNING "idOrador";"""

    elif (op == "09"):     
        sql = """INSERT INTO "TextGrid" ("fechaAlmacenamientoTextGrid", "fechaAlmacenamientoAudio", "idOrador", fichero, tipo) 
                VALUES (%s, %s, %s, %s, %s) RETURNING "idOrador";"""

    conn = None
    id = None
    
    try:
        conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
        cur = conn.cursor()
        
        if (op == "06"):
            cur.execute(sql, datos1)
            id = cur.fetchone()[0]
            conn.commit()
            conn.close()

            if conn is not None:
                conn.close()

            info = datos.split(",")
            info[1] = info[1].replace("\n", "")

            resLocal = info[0].split("-")
            segmentos = info[1].replace(" ", "").split("_")
            
            err = 7

            i = 0
            x = 0

            for resultado in resLocal:
                d = []
                d.append(id)
                d.append(-1)

                if (x % 2) == 0:
                    d.append(segmentos[i] + "-" + segmentos[i] + "*")
                    i += 1
                else:
                    d.append(segmentos[i-1] + "*-" + segmentos[i])

                d.append(resultado.replace("[", "(").replace("]", ")"))

                conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
                cur = conn.cursor()

                sql = """INSERT INTO "Segmento" ("idAnalisisTendenciaTonal", "idAnalisisIntersilabico", valor, resultado) 
                VALUES (%s, %s, %s, %s) RETURNING *;"""

                cur.execute(sql, d)
                conn.commit()
                cur.close()

                if conn is not None:
                    conn.close()

                x += 1

        elif (op == "07"):
            cur.execute(sql, datos1)
            id = cur.fetchone()[0]
            conn.commit()
            conn.close()

            if conn is not None:
                conn.close()

            info = datos.split(",")
            info[1] = info[1].replace("\n", "")

            resLocal = info[0].split("-")
            segmentos = info[1].replace(" ", "").split("_")
            
            err = 7

            i = 0
            x = 0

            for resultado in resLocal:
                d = []
                d.append(-1)
                d.append(id)
                d.append(segmentos[i])
                d.append(resultado)
                i += 1

                conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
                cur = conn.cursor()

                sql = """INSERT INTO "Segmento" ("idAnalisisTendenciaTonal", "idAnalisisIntersilabico", valor, resultado) 
                VALUES (%s, %s, %s, %s) RETURNING *;"""

                cur.execute(sql, d)
                conn.commit()
                cur.close()

                if conn is not None:
                    conn.close()

                x += 1

        elif (op == "08"):
            lineas = info.split(";")
            
            d = []
            i = ""
            dato = ""

            for linea in lineas:
                if (linea != ""):
                    conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
                    cur = conn.cursor()

                    datos = linea.split(",")
                    err = 8
                    
                    datos[0] = datos[0].replace("_", " ")
                    if (datos[5].replace("\n", "") == "Native"):
                        ConsultarDatos.main("07", datos[0])
                    elif (datos[5].replace("\n", "") == "Student"):
                        ConsultarDatos.main("08", datos[0])
                    f = open("Info.txt", "r+")
                    datos[0] = f.read()
                    f.close()

                    datos[1] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
                    datos[2] = datos[2].replace("_", " ")
                    
                    tipoEnunciado = datos[4]
                    ConsultarDatos.main("05", datos[4])
                    f = open("Info.txt", "r+")
                    datos[4] = f.read()
                    f.close()

                    datos[5] = datos[5].replace("\n", "")
                    ConsultarDatos.main("06", datos[5])
                    f = open("Info.txt", "r+")
                    datos[5] = f.read()
                    f.close()

                    cur.execute(sql, datos)
                    id = cur.fetchone()[0]
                    conn.commit()
                    cur.close()

                    i = datos[0] + "," + datos[1] + "," + datos[2] + "," + tipoEnunciado
                    d.append(i)

                    time.sleep(1)

            for x in d:
                dato = dato + x + ";"

            f = open("Info.txt", "w+")
            f.write(dato)
            f.close()

        elif (op == "09"):
            lineas = info.split(";")
            
            for linea in lineas:
                if (linea != ""):
                    conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
                    cur = conn.cursor()

                    linea = linea.split(",")
                    tipo = linea[5].replace("\n", "")
                    linea.remove(tipo)
                    datos = linea
                    err = 9
                    
                    datos[0] = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
                    if (tipo == "Native"):
                        ConsultarDatos.main("07", datos[2])
                    elif (tipo == "Student"):
                        ConsultarDatos.main("08", datos[2])
                    f = open("Info.txt", "r+")
                    datos[2] = f.read()
                    f.close()
                    
                    cur.execute(sql, datos)
                    id = cur.fetchone()[0]
                    conn.commit()
                    cur.close()

        else:
            cur.execute(sql, datos)
            id = cur.fetchone()[0]
            conn.commit()
            cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        err = 4
        print(error)
        path = os.getcwd() + "/ErrorBaseDatos.py"
        
        if (error.pgcode == "23505"):
            os.execv("C:\msys64\mingw64.exe", ["python ", path, " 00 ", datos[0].replace(" ", "_")])

    finally:
        if conn is not None:
            conn.close()

    if ( (err == 0) or (err == 1) or (err == 2) or (err == 3) or (err == 9) ):
        path = os.getcwd() + "/SalidaRegistro.py"
        os.execv("C:\msys64\mingw64.exe", ["python ", path, str(err), datos[0].replace(" ", "_")])

    if (err == 5):
        path = os.getcwd() + "/SalidaAnalisisGlobal.py"
        os.execv("C:\msys64\mingw64.exe", ["python ", path, texto.replace(" ", "_"), datos[0]])

    return id


def main(op):
    with open('Info.txt', encoding='utf-8') as f:
        lines = f.read()

    InsertarDatos(op, lines)    


if __name__ == "__main__":
    main(sys.argv[1])