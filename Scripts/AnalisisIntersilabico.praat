########## FORMULARIO PARA OBTENER LAS CARACTERÍSTICAS DE LOS AUDIOS A COMPARAR ##########
form Audio characteristics
	optionmenu type_of_statement: 1
		option Declarative
		option Desiderative
		option Enumerative
		option Exclamatory
		option Imperative
		option Partial interrogative
		option Alternative total interrogative
		option Polar total interrogative
	choice view_tonal_tendency_curves_in: 1
		button one chart
		button two charts
endform

direccionInfo$ = homeDirectory$ + "/Praat/Scripts/Info.txt"
writeFileLine: direccionInfo$, type_of_statement$
runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/SeleccionFicherosIntersilabico.py"

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroAudioEstudiante.txt"
if fileReadable(direccionInfo$)
    ficheroAudioEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroAudioNativo.txt"
if fileReadable(direccionInfo$)
    ficheroAudioNativo$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroTextGridEstudiante.txt"
if fileReadable(direccionInfo$)
    ficheroTextGridEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\FicheroTextGridNativo.txt"
if fileReadable(direccionInfo$)
    ficheroTextGridNativo$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\DatosEstudiante.txt"
if fileReadable(direccionInfo$)
    datosEstudiante$ = readFile$ (direccionInfo$)
endif

direccionInfo$ = homeDirectory$ + "\Praat\Scripts\DatosNativo.txt"
if fileReadable(direccionInfo$)
    datosNativo$ = readFile$ (direccionInfo$)
endif

ficheroAudioEstudiante = Read from file: ficheroAudioEstudiante$
selectObject: ficheroAudioEstudiante
nombreAudio$ = selected$: "Sound"

ficheroTextGridEstudiante = Read from file: ficheroTextGridEstudiante$
selectObject: ficheroTextGridEstudiante
nombreTextGrid$ = selected$: "TextGrid"

# SE OBTIENEN LOS DATOS DEL AUDIO Y DEL TEXTGRID DEL ESTUDIANTE A ANALIZAR
duracionEstudiante$ = Get total duration
duracion = number(duracionEstudiante$)
plusObject: "Sound " + nombreAudio$
View & Edit

# Se recogen los segmentos en los que se divide el audio
include CalculoIntervalosEjeX.praat

# Se recogen los valores del pitch para cada segmento
include CalculoPitch.praat
direccionVectorEstudiante$ = homeDirectory$ + "/Praat/Scripts/vectorEstudianteAnalisisIntersilabico.txt"
writeFileLine: direccionVectorEstudiante$, vector#

# Se calculan las diferencias locales
include CalculoDiferencias.praat
vectorMediasEstudiante# = vectorMedias#

ficheroAudioNativo = Read from file: ficheroAudioNativo$
selectObject: ficheroAudioNativo
nombreAudio$ = selected$: "Sound"

ficheroTextGridNativo = Read from file: ficheroTextGridNativo$
selectObject: ficheroTextGridNativo
nombreTextGrid$ = selected$: "TextGrid"

# SE OBTIENEN LOS DATOS DEL AUDIO Y DEL TEXTGRID DE REFERENCIA A COMPARAR
duracionReferencia$ = Get total duration
duracion = number(duracionReferencia$)
plusObject: "Sound " + nombreAudio$

View & Edit

# Se recogen los valores del pitch para cada segmento
include CalculoPitch.praat
direccionVectorReferencia$ = homeDirectory$ + "/Praat/Scripts/vectorReferenciaAnalisisIntersilabico.txt"
writeFileLine: direccionVectorReferencia$, vector#

# Se calculan las diferencias locales
include CalculoDiferencias.praat
vectorMediasReferencia# = vectorMedias#


########## SE CALCULA EL PORCENTAJE DE SIMILITUD GLOBAL Y DIFERENCIA MEDIA A PARTIR DE LAS DIFERENCIAS LOCALES EN CADA SÍLABA ##########
diferenciasLocales$ = ""
vectorDiferencias# = zero# (intervalos)
count = 0

for i from 1 to intervalos
	if vectorMediasEstudiante# [i] >= vectorMediasReferencia# [i]
		vectorDiferencias# [i] = round(100 - ( ( vectorMediasReferencia# [i] / vectorMediasEstudiante# [i] ) * 100))
		if i = intervalos
			diferenciasLocales$ = diferenciasLocales$ + string$(vectorDiferencias# [i]) + "%"
		else
			diferenciasLocales$ = diferenciasLocales$ + string$(vectorDiferencias# [i]) + "%" + "-"
		endif
	else
		vectorDiferencias# [i] = round(100 - ( ( vectorMediasEstudiante# [i] / vectorMediasReferencia# [i] ) * 100))
		if i = intervalos
			diferenciasLocales$ = diferenciasLocales$ + string$(vectorDiferencias# [i]) + "%"
		else
			diferenciasLocales$ = diferenciasLocales$ + string$(vectorDiferencias# [i]) + "%" + "-"
		endif
	endif
endfor

diferenciaMedia = sum(vectorDiferencias#) / intervalos
diferenciaMedia$ = fixed$ (diferenciaMedia, 2)
similitud = 100 - diferenciaMedia
similitud$ = fixed$ (similitud, 2)

# SE ALMACENA EL RESULTADO EN LA BASE DE DATOS
direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
info$ = similitud$ + ",fecha," + "grafica," + datosEstudiante$ + "," + datosNativo$ + "," + diferenciaMedia$ + "," + diferenciasLocales$ + "," + intervalosEjeX$ + "," + string$(view_tonal_tendency_curves_in)
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "C:\msys64\mingw64.exe python " + homeDirectory$ + "/Praat/Scripts/SalidaAnalisisIntersilabico.py"