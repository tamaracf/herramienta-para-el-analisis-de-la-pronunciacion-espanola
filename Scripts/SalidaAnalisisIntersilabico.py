#/usr/bin/env python3

import gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


f = open("Info.txt", "r+")
datos = f.read()
f.close()

datos = datos.split(",")
texto1 = datos[10].replace("\n", "")
texto = texto1.split("_")
resLocal = datos[9].split("-")


class ScrolledWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_title("Praat Info: Intersyllabic analysis")
        self.set_default_size(600, 200)
        self.connect("destroy", Gtk.main_quit)

        scrolledwindow = Gtk.ScrolledWindow()
        self.add(scrolledwindow)

        layout = Gtk.Layout()
        layout.set_size(10000, 200)
        layout.set_vexpand(True)
        layout.set_hexpand(True)
        scrolledwindow.add(layout)

        hadjustment = layout.get_hadjustment()
        scrolledwindow.set_hadjustment(hadjustment)
        vadjustment = layout.get_vadjustment()
        scrolledwindow.set_vadjustment(vadjustment)

        grid = Gtk.Grid()

        label00 = Gtk.Label(label="Se indica la diferencia media de pitch en cada silaba:")
        grid.attach(label00, 0, 0, 15, 1)

        x=0
        for i in texto:
            label1 = Gtk.Label(label=i.upper())
            
            if x==0:
                grid.attach(label1, x, 1, 2, 1)
            else:
                grid.attach(label1, x, 1, 2, 1)
            x=x+2
            
        x=0
        for i in resLocal:
            label2 = Gtk.Label(label=i)
            grid.attach(label2, x, 2, 2, 2)
            x=x+2
        
        label3 = Gtk.Label(label="")
        grid.attach(label3, 0, 3, 1, 1)
        label4 = Gtk.Label(label="")
        grid.attach(label4, 0, 4, 1, 1)

        label5 = Gtk.Label(label="Similitud: " + datos[0] + "%")
        grid.attach(label5, 0, 5, 4, 1)
        
        label6 = Gtk.Label(label="Diferencia media: " + datos[8] + "%")
        grid.attach(label6, 0, 6, 4, 1)

        layout.add(grid)

window = ScrolledWindow()
window.show_all()

Gtk.main()

path = os.getcwd() + r'\Grafica.py '
os.execv("C:\msys64\mingw64.exe ", ["python ", path, " 1 ", texto1, datos[11]])