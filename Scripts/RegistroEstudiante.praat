# FORMULARIO PARA OBTENER LA INFORMACIÓN RELEVANTE DEL ESTUDIANTE
form Student characteristics
	sentence name_and_last_name
	sentence username
	choice gender: 1
		button Female
		button Male
	sentence age
	optionmenu level: 4
		option A1
		option A2
		option B1
		option B2
		option C1
		option C2
endform


direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Info.txt"
nombreCompleto$ = replace$(name_and_last_name$, " ", "_", 0)
info$ = username$ + "," + nombreCompleto$ + "," + age$ + "," + "docente" + "," + gender$ + "," + level$
writeFileLine: direccionInfo$, info$

runSystem_nocheck: "python " + homeDirectory$ + "/Praat/Scripts/RegistroEstudiante.py"