direccionInfo$ = homeDirectory$ + "\Praat\Scripts\Fichero.txt"

if fileReadable(direccionInfo$)
    info$ = readFile$ (direccionInfo$)
endif

fichero = Read from file: info$

selectObject: fichero
nombreAudio$ = selected$: "Sound"
writeInfoLine: nombreAudio$

# SE OBTIENEN LOS DATOS DEL AUDIO DEL ESTUDIANTE A ANALIZAR
duracion$ = Get total duration
duracionEstudiante = number(duracion$)

View & Edit

# El audio se divide en 1000 puntos
puntos = 1000

tiempo = number(fixed$(duracionEstudiante, 4) ) 

# Se eliminan los silencios iniciales y finales
include prelimpiadoFicheroWAV.praat

vectorEstudiante# = vector#
direccionVectorEstudiante$ = homeDirectory$ + "/Praat/Scripts/vectorEstudianteAnalisisGlobal.txt"
writeFileLine: direccionVectorEstudiante$, vectorEstudiante#