# SE OBTIENEN LAS ETIQUETAS DE CADA INTERVALO
editor: "TextGrid " +  nombreTextGrid$

	info$ = TextGrid info
	intervalos = number(extractLine$(info$, "Number of intervals:"))
	intervalosEjeX$ = ""
	Move cursor to: 0.0

	for i from 1 to intervalos
		etiqueta$ = Get label of interval
		if i = intervalos
			intervalosEjeX$ = intervalosEjeX$ + etiqueta$
		else
			intervalosEjeX$ = intervalosEjeX$ + etiqueta$ + "_"
		endif
		Select next interval
	endfor
	
endeditor