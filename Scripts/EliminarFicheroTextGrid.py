#!/usr/bin/python3

import os, subprocess, ConsultarDatos, EliminarDatos, GoogleDrive


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
lines = f.read()
f.close()

info = lines.split(",")
info[2] = info[2].replace("_", " ").replace("\n", "")

ConsultarDatos.main("04", info[0])

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

filtro = info[0] + "," + orador + "," + info[1] + "," + info[2]
ConsultarDatos.main("12", filtro)

path = os.getcwd() + "\SeleccionTextGrid.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Audios.txt", "r+")
textgrid = f.read()
f.close()

x = textgrid.index(" - ")
texto = textgrid[0:x]
x = x+4
fechas = textgrid[x:len(textgrid)]

y = fechas.index(" - ")
fechaAudio = fechas[0:y]
y = y+4
fechaTextGrid = fechas[y:len(fechas)]

if (info[0] == "Native"):
	ConsultarDatos.main("07", orador.replace("_", " "))
elif (info[0] == "Student"):
	ConsultarDatos.main("08", orador.replace("_", " "))

f = open("Info.txt", "r+")
idOrador = f.read()
f.close()

d = idOrador + "," + fechaAudio + "," + fechaTextGrid
ConsultarDatos.main("13", d)
f = open("Info.txt", "r+")
fichero = f.read()
f.close()

if (info[0] == "Native"):
	GoogleDrive.main("3")
elif (info[0] == "Student"):
	os.remove(fichero)

f = open("Info.txt", "w+")
f.write(d)
f.close()

EliminarDatos.main("05")