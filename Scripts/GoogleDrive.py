#!/usr/bin/env python3

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import sys, InsertarDatos, EliminarDatos, os, random

directorio_credenciales = 'credentials.json'

# INICIAR SESIÓN
def login():
    GoogleAuth.DEFAULT_SETTINGS['client_config_file'] = directorio_credenciales
    gauth = GoogleAuth()
    gauth.LoadCredentialsFile(directorio_credenciales)
    
    if gauth.credentials is None:
        gauth.LocalWebserverAuth(port_numbers=[8092])
    elif gauth.access_token_expired:
        gauth.Refresh()
    else:
        gauth.Authorize()
        
    gauth.SaveCredentialsFile(directorio_credenciales)
    credenciales = GoogleDrive(gauth)

    return credenciales


# SUBIR UN ARCHIVO A DRIVE
def subir_archivo(op, ruta_archivo, id_folder):
    credenciales = login()
    archivo = credenciales.CreateFile({'parents': [{"kind": "drive#fileLink", "id": id_folder}]})
    if (op == "0"):
        archivo['title'] = str(random.randint(0, 1000000000000)) + ".wav"
    elif (op == "1"):
        archivo['title'] = str(random.randint(0, 1000000000000)) + ".TextGrid"
    archivo.SetContentFile(ruta_archivo)
    
    archivo.Upload()

    f = open("Info.txt", "r+")
    datos = f.read()
    f.close()
    datos = datos.split(",")
    datos[3] = archivo['id']
    
    info = ""
    i = 0
    for dato in datos:
        if (i == 0):
            info = info + dato
            i += 1
        else:
            info = info + "," + dato
    
    f = open("Info.txt", "w+")
    f.write(info)
    f.close()


def eliminar_archivo(id_archivo):
    credenciales = login()
    archivo = credenciales.CreateFile({'id': id_archivo})
    archivo.Trash()
    archivo.Delete()


def bajar_archivo_por_id(id_archivo, ruta_descarga):
    credenciales = login()
    archivo = credenciales.CreateFile({'id': id_archivo}) 
    nombre_archivo = archivo['title']
    ruta = ruta_descarga + "/" + nombre_archivo
    ruta = ruta.replace("\\", "/")

    f = open("Info.txt", "w+")
    f.write(ruta)
    f.close()

    archivo.GetContentFile(ruta)


def main(op):
    with open('Info.txt', encoding='utf-8') as f:
        lines = f.read()

    info = lines.split(",")
    
    if (op == "0"):
        id_folder = '1XcjrAf6h64yuiDGHDOns7N9o3ggGt5EU'
        subir_archivo(op, info[3], id_folder)
        InsertarDatos.main("03")

    elif (op == "1"):
        id_folder = '13dMLkKih8C93bbbpGPihmE47y21Eizzv'
        subir_archivo(op, info[3], id_folder)
        InsertarDatos.main("04")
    
    elif (op == "2"):
        eliminar_archivo(info[0])
    
    elif (op == "3"):
        eliminar_archivo(info[0])
    
    elif (op == "4"):
        ruta = os.getcwd().replace("Scripts", "Files")
        bajar_archivo_por_id(info[0], ruta)
        path = os.getcwd() + "\Control.txt"
        f = open(path, "w+")
        f.write("OK")
        f.close()

    elif (op == "5"):
        f = open("Info.txt", "r+")
        ficheros = f.read()
        f.close()
        
        ficheros = ficheros.split(",")
        for fichero in ficheros:
            if (fichero != ""):
                eliminar_archivo(fichero)


if __name__ == "__main__":
    main(sys.argv[1])