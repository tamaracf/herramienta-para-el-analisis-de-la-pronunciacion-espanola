#!/usr/bin/python3

import psycopg2, sys, os, ConsultarDatos


# Eliminar información de la base de datos
def EliminarDatos(op, info):
    sql = ""
    rows_deleted = 0
    
    if (op == "00"):
        sql = """DELETE FROM "Docente" WHERE "nombreUsuario"=%s RETURNING *;"""
        info = info.replace(" ", "").replace("\n", "")
    
    elif (op == "01"):
        sql = """DELETE FROM "Estudiante" WHERE "nombreUsuario"=%s RETURNING *;"""
        info = info.replace(" ", "").replace("\n", "")

    elif (op == "02"):
        sql = """DELETE FROM "Nativo" WHERE "nombreCompleto"=%s RETURNING *;"""
        info = info.replace("_", " ").replace("\n", "")
    
    elif (op == "03"):
        import GoogleDrive
        sql = """DELETE FROM "Audio" WHERE "idOrador"=%s AND "fechaAlmacenamiento"=%s AND "texto"=%s RETURNING *;"""
        info = info.replace("_", " ").replace("\n", "")
        datos = info.split(",")
        tipoOrador = datos[3]
        datos.remove(tipoOrador)
    
    elif (op == "05"):
        import GoogleDrive
        sql = """DELETE FROM "TextGrid" WHERE "idOrador"=%s AND "fechaAlmacenamientoAudio"=%s AND "fechaAlmacenamientoTextGrid"=%s RETURNING *;"""
        info = info.replace("_", " ").replace("\n", "")
        datos = info.split(",")


    conn = None
    
    try:
        conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
        cur = conn.cursor()
        
        if (op == "03"):
            filtro = datos[0] + "," + datos[1].replace(" ", "_")
            ConsultarDatos.main("16", filtro)

            if (tipoOrador == "Native"):
                GoogleDrive.main("5")

            elif (tipoOrador == "Student"):
                f = open("Info.txt", "r+")
                ficheros = f.read()
                f.close()
                
                ficheros = ficheros.split(",")
                for fichero in ficheros:
                    if (fichero != ""):
                        os.remove(fichero)

            cur.execute(sql, datos)
            rows_deleted = cur.rowcount

            conn.commit()
            cur.close()
            conn.close()

            conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
            cur = conn.cursor()

            sql = """DELETE FROM "TextGrid" WHERE "idOrador"=%s AND "fechaAlmacenamientoAudio"=%s RETURNING *;"""

            texto = datos[2]
            info = texto
            datos.remove(texto)
            d = datos
            
            cur.execute(sql, d)
        
        elif (op == "05"):
            cur.execute(sql, (datos[0], datos[1], datos[2]))
            info = ""
            rows_deleted = cur.rowcount

        else:
            cur.execute(sql, (info,))
            rows_deleted = cur.rowcount
        
        conn.commit()
        cur.close()
        
        if (rows_deleted == 0):
            path = os.getcwd() + "/ErrorBaseDatos.py"
            os.execv("C:\msys64\mingw64.exe", ["python ", path, " 01 ", info.replace(" ", "_")])
        else:
            path = os.getcwd() + "/SalidaBaja.py"
            os.execv("C:\msys64\mingw64.exe", ["python ", path, op, info.replace(" ", "_")])

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        
    finally:
        if conn is not None:
            conn.close()       


def main(op):
    with open('Info.txt', encoding='utf-8') as f:
        lines = f.read()

    EliminarDatos(op, lines)


if __name__ == "__main__":
    main(sys.argv[1])