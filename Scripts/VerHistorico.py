#!/usr/bin/python3

import os, subprocess, ConsultarDatos


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

ConsultarDatos.main("04", "Student")

path = os.getcwd() + "\FormularioHistorial.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
orador = f.read()
f.close()

f = open("Info.txt", "r+")
tipo = f.read()
f.close()

datos = orador + "," + tipo
ConsultarDatos.main("15", datos)

path = os.getcwd() + "\SalidaVerHistorico.py"
arg = [r'C:\msys64\mingw64.exe', "python", path, orador.replace(" ", "_"), tipo.replace(" ", "_")]
subprocess.call(arg)

path = os.getcwd() + "\GraficaEvolutiva.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.call(arg)