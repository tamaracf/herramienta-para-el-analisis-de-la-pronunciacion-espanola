#!/usr/bin/python3

import psycopg2, sys


# Consultar información de la base de datos
def ConsultarDatos(op, dato):
    datos = []
    sql = ""
    info = ""
    
    if (op == "00"):
        sql = """SELECT "nombreUsuario" FROM "Docente";"""

    elif (op == "01"):
        sql = """SELECT id FROM "GeneroOrador" WHERE genero = %s;"""

    elif (op == "02"):
        sql = """SELECT id FROM "NivelOrador" WHERE nivel=%s;"""
    
    elif (op == "03"):
        sql = """SELECT id FROM "Docente" WHERE "nombreUsuario"=%s;"""

    elif (op == "04"):
        if (dato.replace("\n", "") == "Native"):
            sql = """SELECT "nombreCompleto" FROM "Nativo";"""
        elif (dato.replace("\n", "") == "Student"):
            sql = """SELECT "nombreUsuario" FROM "Estudiante";"""
    
    elif (op == "05"):
        sql = """SELECT id FROM "TipoEnunciado" WHERE "tipo"=%s;"""

    elif (op == "06"):
        sql = """SELECT id FROM "TipoAudio" WHERE "tipo"=%s;"""

    elif (op == "07"):
        sql = """SELECT id FROM "Nativo" WHERE "nombreCompleto"=%s;"""

    elif (op == "08"):
        sql = """SELECT id FROM "Estudiante" WHERE "nombreUsuario"=%s;"""
    
    elif (op == "09"):
        datos = dato.split(",")
        if (datos[0] == "Native"):
            ConsultarDatos("07", datos[1].replace("_", " "))
            f = open("Info.txt", "r+")
            datos[1] = f.read()
            f.close()
        elif (datos[0] == "Student"):
            ConsultarDatos("08", datos[1].replace("_", " "))
            f = open("Info.txt", "r+")
            datos[1] = f.read()
            f.close()

        ConsultarDatos("05", datos[2])
        f = open("Info.txt", "r+")
        datos[2] = f.read()
        f.close()
        
        sql = """SELECT texto, "fechaAlmacenamiento" FROM "Audio" WHERE "idOrador"=%s AND "tipoEnunciado"=%s;"""

    elif (op == "10"):
        sql = """SELECT id FROM "TipoTextGrid" WHERE "tipo"=%s;"""

    elif (op == "11"):
        sql = """SELECT fichero FROM "Audio" WHERE "fechaAlmacenamiento"=%s AND texto=%s;"""

    elif (op == "12"):
        datos = dato.split(",")
        if (datos[0] == "Native"):
            ConsultarDatos("07", datos[1].replace("_", " "))
            f = open("Info.txt", "r+")
            datos[1] = f.read()
            f.close()
        elif (datos[0] == "Student"):
            ConsultarDatos("08", datos[1].replace("_", " "))
            f = open("Info.txt", "r+")
            datos[1] = f.read()
            f.close()

        ConsultarDatos("05", datos[2])
        f = open("Info.txt", "r+")
        datos[2] = f.read()
        f.close()

        ConsultarDatos("06", datos[0])
        f = open("Info.txt", "r+")
        datos[0] = f.read()
        f.close()

        ConsultarDatos("10", datos[3])
        f = open("Info.txt", "r+")
        datos[3] = f.read()
        f.close()

        sql = """SELECT texto, "fechaAlmacenamientoAudio", "fechaAlmacenamientoTextGrid" FROM "Audio" JOIN "TextGrid" 
                ON "Audio"."idOrador"="TextGrid"."idOrador" AND "fechaAlmacenamiento"="fechaAlmacenamientoAudio" WHERE 
                "tipoAudio"=%s AND "Audio"."idOrador"=%s AND "tipoEnunciado"=%s AND tipo=%s;"""

    elif (op == "13"):
        sql = """SELECT fichero FROM "TextGrid" WHERE "idOrador"=%s AND "fechaAlmacenamientoAudio"=%s AND "fechaAlmacenamientoTextGrid"=%s;"""
    
    elif (op == "14"):
        datos = dato.split(",")

        ConsultarDatos("05", datos[2])
        f = open("Info.txt", "r+")
        datos[2] = f.read()
        f.close()

        ConsultarDatos("06", datos[0])
        f = open("Info.txt", "r+")
        datos[0] = f.read()
        f.close()

        ConsultarDatos("10", datos[4])
        f = open("Info.txt", "r+")
        datos[4] = f.read()
        f.close()

        sql = """SELECT texto, "fechaAlmacenamientoAudio", "fechaAlmacenamientoTextGrid" FROM "Audio" JOIN "TextGrid" 
                ON "Audio"."idOrador"="TextGrid"."idOrador" AND "fechaAlmacenamiento"="fechaAlmacenamientoAudio" WHERE 
                "tipoAudio"=%s AND "Audio"."idOrador"=%s AND "tipoEnunciado"=%s AND texto=%s AND tipo=%s;"""

    elif (op == "15"):
        datos = dato.split(",")
        
        if (datos[1] == "Global analysis"):
            sql = """SELECT "AnalisisGlobal".id, fecha, texto, "TipoEnunciado".tipo, "similitudGlobal" FROM "AnalisisGlobal", "Estudiante", 
                    "Audio", "TipoEnunciado" WHERE "AnalisisGlobal"."idReferencia"="Audio"."idOrador" AND 
                    "AnalisisGlobal"."fechaAlmacenamientoAudioReferencia"="Audio"."fechaAlmacenamiento" AND "Estudiante".id="Audio"."idOrador" AND 
                    "TipoEnunciado".id="Audio"."tipoEnunciado" AND "nombreUsuario"=%s;"""

        elif (datos[1] == "Tonal tendency analysis"):
            sql = """SELECT "AnalisisTendenciaTonal".id, fecha, texto, "TipoEnunciado".tipo, "similitudGlobal" FROM "AnalisisTendenciaTonal", 
                    "Estudiante", "Audio", "TipoEnunciado" WHERE "AnalisisTendenciaTonal"."idEstudiante"="Audio"."idOrador" AND 
                    "AnalisisTendenciaTonal"."fechaAlmacenamientoAudioEstudiante"="Audio"."fechaAlmacenamiento" AND "Estudiante".id="AnalisisTendenciaTonal"."idEstudiante" AND 
                    "TipoEnunciado".id="Audio"."tipoEnunciado" AND "nombreUsuario"=%s;"""
            
        elif (datos[1] == "Intersyllabic analysis"):
            sql = """SELECT "AnalisisIntersilabico".id, fecha, texto, "TipoEnunciado".tipo, "similitudGlobal", "diferenciaMedia" FROM "AnalisisIntersilabico", "Estudiante", 
                    "Audio", "TipoEnunciado" WHERE "AnalisisIntersilabico"."idEstudiante"="Audio"."idOrador" AND 
                    "AnalisisIntersilabico"."fechaAlmacenamientoAudioEstudiante"="Audio"."fechaAlmacenamiento" AND "Estudiante".id="AnalisisIntersilabico"."idEstudiante" AND 
                    "TipoEnunciado".id="Audio"."tipoEnunciado" AND "nombreUsuario"=%s;"""

    elif (op == "16"):
        datos = dato.split(",")
        datos[1] = datos[1].replace("_", " ")
        sql = """SELECT fichero FROM "TextGrid" WHERE "idOrador"=%s AND "fechaAlmacenamientoAudio"=%s;"""

    elif (op == "17"):
        dato = dato.split(",")
        if (dato[0].replace("\n", "") == "Native"):
            ConsultarDatos("07", dato[1].replace("\n", ""))
        elif (dato[0].replace("\n", "") == "Student"):
            ConsultarDatos("08", dato[1].replace("\n", ""))

        f = open("Info.txt", "r+")
        idOrador = f.read()
        f.close()
        
        sql = """SELECT "Audio"."tipoEnunciado", "Audio".texto, "Audio"."fechaAlmacenamiento", "TextGrid"."fechaAlmacenamientoTextGrid", "TextGrid".tipo 
                FROM "Audio" LEFT JOIN "TextGrid" ON "Audio"."fechaAlmacenamiento"="TextGrid"."fechaAlmacenamientoAudio" AND "Audio"."idOrador"="TextGrid"."idOrador" 
                WHERE "Audio"."idOrador"=%s;"""

    elif (op == "18"):
        sql = """SELECT tipo FROM "TipoEnunciado" WHERE id=%s;"""

    elif (op == "19"):
        sql = """SELECT tipo FROM "TipoTextGrid" WHERE id=%s;"""

    conn = None
    
    try:
        conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
        cur = conn.cursor()

        if (op == "00"):
            cur.execute(sql,)
            i = 0
            datos = cur.fetchall()
            for x in datos:
                if (i == 0):
                    info = x[0].replace(" ", "").replace("\n", "")
                    i += 1
                else:
                    info = info + "," + x[0]
            
            f = open("Docentes.txt", "w+")
            f.write(info)
            f.close()

        elif (op == "04"):
            cur.execute(sql,)
            i = 0
            datos = cur.fetchall()
            for x in datos:
                if dato.replace("\n", "") == "Student":
                    if (i == 0):
                        info = x[0].replace(" ", "").replace("\n", "")
                        i += 1
                    else:
                        info = info + "," + x[0]
                elif dato.replace("\n", "") == "Native":
                    if (i == 0):
                        info = x[0].replace("\n", "")
                        i += 1
                    else:
                        info = info + "," + x[0]
            
            f = open("Oradores.txt", "w+")
            f.write(info)
            f.close()

        elif (op == "09"):
            cur.execute(sql, (datos[1],datos[2]))
            i = 0
            datos = cur.fetchall()
            info = []
            
            for dato in datos:
                x = []
                x.append(dato[0])
                x.append(dato[1].strftime("%Y-%m-%d %H:%M:%S"))
                info.append(x)

            f = open("Info.txt", "w+")
            f.write(str(info))
            f.close()

        elif ((op == "11") or (op == "13")):
            datos = dato.split(",")
            cur.execute(sql, datos)
            info = cur.fetchone()

            f = open("Info.txt", "w+")
            f.write(str(info[0]))
            f.close()

        elif (op == "12"):
            cur.execute(sql, (datos[0],datos[1], datos[2], datos[3]))
            i = 0
            datos = cur.fetchall()
            info = []
            
            for dato in datos:
                x = []
                x.append(dato[0])
                x.append(dato[1].strftime("%Y-%m-%d %H:%M:%S"))
                x.append(dato[2].strftime("%Y-%m-%d %H:%M:%S"))
                info.append(x)
            
            f = open("Info.txt", "w+")
            f.write(str(info))
            f.close()

        elif (op == "14"):
            cur.execute(sql, (datos[0],datos[1], datos[2], datos[3], datos[4]))
            i = 0
            datos = cur.fetchall()
            info = []
            
            for dato in datos:
                x = []
                x.append(dato[0])
                x.append(dato[1].strftime("%Y-%m-%d %H:%M:%S"))
                x.append(dato[2].strftime("%Y-%m-%d %H:%M:%S"))
                info.append(x)
            
            f = open("Info.txt", "w+")
            f.write(str(info))
            f.close()

        elif (op == "15"):
            ids = ""
            cur.execute(sql, (datos[0],))
            filas = cur.fetchall()
            
            for fila in filas:
                if (datos[1] == "Global analysis"):
                    info = info + str(fila[0]) + "-"
                    info = info + fila[1].strftime("%Y/%m/%d %H:%M:%S") + "-"
                    info = info + fila[2] + "-"
                    info = info + fila[3].rstrip() + "-"
                    info = info + str(fila[4]).rstrip() + "|"
                elif (datos[1] == "Tonal tendency analysis"):
                    ids = ids + str(fila[0]) + ","
                    info = info + str(fila[0]) + "-"
                    info = info + fila[1].strftime("%Y/%m/%d %H:%M:%S") + "-"
                    info = info + fila[2] + "-"
                    info = info + fila[3].rstrip() + "-"
                    info = info + str(fila[4]).rstrip() + "|"
                elif (datos[1] == "Intersyllabic analysis"):
                    ids = ids + str(fila[0]) + ","
                    info = info + str(fila[0]) + "-"
                    info = info + fila[1].strftime("%Y/%m/%d %H:%M:%S") + "-"
                    info = info + fila[2] + "-"
                    info = info + fila[3].rstrip() + "-"
                    info = info + str(fila[4]).rstrip() + "-"
                    info = info + str(fila[5]).rstrip() + "|"

            f = open("Historico.txt", "w+")
            f.write(info)
            f.close()
        
        elif (op == "16"):
            cur.execute(sql, datos)
            filas = cur.fetchall()
            info = ""
            
            for fila in filas:
                info = info + fila[0] + ","
             
            f = open("Info.txt", "w+")
            f.write(str(info))
            f.close()

        elif (op == "17"):
            cur.execute(sql, (idOrador,))
            filas = cur.fetchall()
            info = dato[1] + "|"
            count = 0
            
            for fila in filas:
                if (count == 0):
                    info = info + str(fila[0]) + ";" + str(fila[1]) + "," + str(fila[2]) + ";" + str(fila[3]) + "," + str(fila[4])
                else:
                    info = info + "|" + str(fila[0]) + ";" + str(fila[1]) + "," + str(fila[2]) + ";" + str(fila[3]) + "," + str(fila[4])
                count = count + 1
            
            f = open("Info.txt", "w+")
            f.write(info)
            f.close()

        else:
            cur.execute(sql, (dato.replace("\n", ""),))
            info = cur.fetchone()[0]  
            
            f = open("Info.txt", "w+")
            f.write(str(info))
            f.close()

        conn.commit()
        cur.close()

        if ((op == "15") and ((datos[1] == "Tonal tendency analysis") or (datos[1] == "Intersyllabic analysis"))):
            conn = psycopg2.connect(host="localhost", database="Praat", user="postgres", password="pass")
            cur = conn.cursor()
            
            info = ""
            ids = ids.split(",")
            ids = ids[:-1]
            for id in ids:
                if (datos[1] == "Tonal tendency analysis"):
                    sql = """SELECT valor, resultado FROM "Segmento" WHERE "idAnalisisTendenciaTonal"=%s;"""
                elif (datos[1] == "Intersyllabic analysis"):
                    sql = """SELECT valor, resultado FROM "Segmento" WHERE "idAnalisisIntersilabico"=%s;"""

                cur.execute(sql, (int(id),))
                filas = cur.fetchall()
                for fila in filas:
                    info = info + fila[0] + "_"
                    info = info + fila[1] + ":"
                info = info + "|"

            f = open("Segmentos.txt", "w+")
            f.write(info)
            f.close()

            conn.commit()
            cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()


def main(op, dato):
    ConsultarDatos(op, dato)    


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])