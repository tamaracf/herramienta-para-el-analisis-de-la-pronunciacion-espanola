#!/usr/bin/python

import gi, os, subprocess, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class ComboBoxWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Select a teacher:")

        self.set_border_width(10)
        self.set_default_size(400, 50)
        
        store = Gtk.ListStore(str)
        for docente in docentes:
            store.append([docente])

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        combo = Gtk.ComboBox.new_with_model(store)
        combo.connect("changed", self.on_combo_changed)
        renderer_text = Gtk.CellRendererText()
        combo.pack_start(renderer_text, True)
        combo.add_attribute(renderer_text, "text", 0)
        vbox.pack_start(combo, False, False, True)
        self.add(vbox)

    def on_combo_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            docente = model[tree_iter][0]
            path = os.getcwd() + "\Docentes.txt"
            f = open(path, "w+")
            f.write(str(docente))
            f.close()
            path = os.getcwd() + "\Control.txt"
            f = open(path, "w+")
            f.write("OK")
            f.close()
            Gtk.Window.destroy(self)


def main():
    global docentes
    with open(os.getcwd() + r'\Docentes.txt', encoding='ISO-8859-1') as f:
        docentes = f.read()

    if ((docentes.replace(" ", "").replace("\n", "") == "[]") or docentes.replace(" ", "").replace("\n", "") == ""):
        path = os.getcwd() + "\Control.txt"
        f = open(path, "w+")
        f.write("NO")
        f.close()
        path = os.getcwd() + "\SalidaNoDatos.py"
        arg = [r'C:\msys64\mingw64.exe', "python", path]
        subprocess.call(arg)
        sys.exit()

    docentes = docentes.replace("\n", "").replace(" ", "").split(",")

    win = ComboBoxWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()