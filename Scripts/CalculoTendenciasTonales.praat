# SE NORMALIZAN LOS VALORES DE PITCH
vectorNormalizado# = zero# (intervalos)
for i from 1 to intervalos
	if (vector# [i] <> undefined)
		vectorNormalizado# [i] = (vector# [i] - min) / (max - min)
	else
		vectorNormalizado# [i] = vector# [i]
	endif
endfor

	
# SE CALCULA LA TENDENCIA DEL TONO EN CADA INTERVALO
intervalos = intervalos - 1
tendencia# = zero# (intervalos)

for  i from 1 to intervalos
	if vectorNormalizado# [i+1] > vectorNormalizado# [i]
		umbral = vectorNormalizado# [i+1] / 10
		if (vectorNormalizado# [i+1] - vectorNormalizado# [i]) > umbral
		 	# TENDENCIA ASCENDENTE
			tendencia# [i] = 2
		else
			# TENDENCIA PLANA
			tendencia# [i] = 1
		endif
	else
		umbral = vectorNormalizado# [i] / 10
		if (vectorNormalizado# [i] - vectorNormalizado# [i+1]) > umbral
		 	# TENDENCIA DESCENDENTE
			tendencia# [i] = 0
		else
			# TENDENCIA PLANA
			tendencia# [i] = 1
		endif
	endif
endfor
