#!/usr/bin/env python3

import gi, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


if (sys.argv[1] == "0"): 
	message = "Teacher " + sys.argv[2] + " was created successfully."
elif (sys.argv[1] == "1"):
	message = message = "Student " + sys.argv[2] + " was created successfully."
elif (sys.argv[1] == "2"):
	message = message = "Local speaker " + sys.argv[2].replace("_", " ") + " was created successfully."
elif (sys.argv[1] == "3"):
	message = message = "File uploaded successfully."
elif (sys.argv[1] == "9"):
	message = message = "Files uploaded successfully."


class MessageDialogWindow(Gtk.Window):
    def __init__(self):
        dialog = Gtk.MessageDialog(
            parent=None,
            flags=0,
            message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK,
            text="Praat Info",
        )
        dialog.format_secondary_text(
            message
        )
        dialog.run()

        dialog.destroy()


win = MessageDialogWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()