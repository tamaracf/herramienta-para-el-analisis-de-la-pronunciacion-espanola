#!/usr/bin/python3

import os, subprocess, ConsultarDatos, GoogleDrive, InsertarDatos, random, parselmouth


if not (os.path.isfile("Control.txt")):
	open("Control.txt", "x")

f = open("Info.txt", "r+")
lines = f.read()
f.close()

info = lines.split(",")
info[1] = info[1].replace("_", " ")
info[2] = info[2].replace("_", " ")
info[4] = info[4].replace("\n", "")

ConsultarDatos.main("04", info[0])

path = os.getcwd() + "\SeleccionOrador.py"
arg = [r'C:\msys64\mingw64.exe', "python", path]
subprocess.run(arg)

control = ""
while (control == ""):
	f = open("Control.txt", "r+")
	control = f.read()
	f.close()

f = open("Control.txt", "w+")
control = f.write("")
f.close()

f = open("Oradores.txt", "r+")
info[3] = f.read()
f.close()

if (info[0] == "Student"):
	path = os.getcwd()
	path = path.replace("Scripts", "Files/")
	if not (os.path.isdir(path)):
		os.mkdir(path)
	fichero = info[4]
	info[4] = path + str(random.randint(0, 1000000000000)) + ".WAV"

datos = info[3] + "," + "fecha" + "," + info[2] + "," + info[4] + "," + info[1] + "," + info[0]


# SE ALMACENA EL AUDIO EN GOOGLE DRIVE Y EN LA BASE DE DATOS
# Nativo
if (info[0] == "Native"):
	f = open("Info.txt", "w+")
	f.write(datos)
	f.close()
	GoogleDrive.main("0")

# Estudiante
elif (info[0] == "Student"):
	f = open("Info.txt", "w+")
	f.write(datos)
	f.close()

	f = open("Fichero.txt", "w+")
	f.write(info[4] + "," + fichero)
	f.close()

	output_string = parselmouth.praat.run_file('GuardarFichero.praat')

	InsertarDatos.main("03")